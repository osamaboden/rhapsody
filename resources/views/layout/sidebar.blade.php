<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <img src="{{ asset('assets/img/logo/logo_rhapsody.png') }}" title="logo_rhapsody" alt="logo">
        </div>
        <ul class="sidebar-menu">
            <li class="{{ findActiveSidebar('active', ['dashboard.index']) }}">
                <a class="nav-link" href="{{route('dashboard.index')}}">
                    <svg width="30" class="mr-3" height="30" viewBox="0 0 20 20" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M18.3333 7.10008V3.31675C18.3333 2.14175 17.8 1.66675 16.475 1.66675H13.1083C11.7833 1.66675 11.25 2.14175 11.25 3.31675V7.09175C11.25 8.27508 11.7833 8.74175 13.1083 8.74175H16.475C17.8 8.75008 18.3333 8.27508 18.3333 7.10008Z"
                            stroke="#898989" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                        <path
                            d="M18.3333 16.475V13.1083C18.3333 11.7833 17.8 11.25 16.475 11.25H13.1083C11.7833 11.25 11.25 11.7833 11.25 13.1083V16.475C11.25 17.8 11.7833 18.3333 13.1083 18.3333H16.475C17.8 18.3333 18.3333 17.8 18.3333 16.475Z"
                            stroke="#898989" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                        <path
                            d="M8.75033 7.10008V3.31675C8.75033 2.14175 8.21699 1.66675 6.89199 1.66675H3.52533C2.20033 1.66675 1.66699 2.14175 1.66699 3.31675V7.09175C1.66699 8.27508 2.20033 8.74175 3.52533 8.74175H6.89199C8.21699 8.75008 8.75033 8.27508 8.75033 7.10008Z"
                            stroke="#898989" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                        <path
                            d="M8.75033 16.475V13.1083C8.75033 11.7833 8.21699 11.25 6.89199 11.25H3.52533C2.20033 11.25 1.66699 11.7833 1.66699 13.1083V16.475C1.66699 17.8 2.20033 18.3333 3.52533 18.3333H6.89199C8.21699 18.3333 8.75033 17.8 8.75033 16.475Z"
                            stroke="#898989" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
                    </svg>

                    <span>Dashboard</span>
                </a>
            </li>

            @if (auth()->user()->type == "admin")
            <li class="{{ findActiveSidebar('active', ['qrcode.index']) }}">
                <a class="nav-link" href="{{route('qrcode.index')}}">
                    <svg width="30" height="30" class="mr-3" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M21.25 25.625H8.75C5 25.625 2.5 23.75 2.5 19.375V10.625C2.5 6.25 5 4.375 8.75 4.375H21.25C25 4.375 27.5 6.25 27.5 10.625V19.375C27.5 23.75 25 25.625 21.25 25.625Z" stroke="#898989" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M7.5 10V20" stroke="#898989" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M11.25 10V15" stroke="#898989" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M11.25 18.75V20" stroke="#898989" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M18.75 10V11.25" stroke="#898989" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M15 10V20" stroke="#898989" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M18.75 15V20" stroke="#898989" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M22.5 10V20" stroke="#898989" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                    <span>QR Code</span>
                </a>
            </li>
            @endif

            <li class="{{ findActiveSidebar('active', ['pembelian_ots.index']) }}">
                <a class="nav-link" href="{{route('pembelian_ots.index')}}">
                    <svg width="30" height="30" class="mr-3" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M26.2626 14.0249V19.6374C26.2626 25.2499 24.0251 27.4999 18.4001 27.4999H11.6626C10.9376 27.4999 10.2751 27.4625 9.6626 27.375" stroke="#898989" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M3.7998 19.4V14.0249" stroke="#898989" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M15.0374 15C17.3249 15 19.0124 13.1376 18.7874 10.8501L17.9498 2.5H12.1124L11.2749 10.8501C11.0499 13.1376 12.7499 15 15.0374 15Z" stroke="#898989" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M22.9124 15C25.4374 15 27.2874 12.95 27.0374 10.4375L26.6874 6.99997C26.2374 3.74997 24.9874 2.5 21.7124 2.5H17.8999L18.7749 11.2625C18.9999 13.325 20.8499 15 22.9124 15Z" stroke="#898989" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M7.08724 15C9.14974 15 11.0122 13.325 11.2122 11.2625L11.4873 8.50006L12.0872 2.5H8.27476C4.99976 2.5 3.74978 3.74997 3.29978 6.99997L2.94975 10.4375C2.69975 12.95 4.56224 15 7.08724 15Z" stroke="#898989" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M11.25 23.75C11.25 24.6875 10.9875 25.5751 10.525 26.3251C10.2875 26.7251 9.99998 27.0875 9.66248 27.375C9.62498 27.425 9.58751 27.4625 9.53751 27.5C8.66251 28.2875 7.5125 28.75 6.25 28.75C4.725 28.75 3.36246 28.0625 2.46246 26.9875C2.43746 26.95 2.40003 26.925 2.37503 26.8875C2.22503 26.7125 2.08752 26.5251 1.97502 26.3251C1.51252 25.5751 1.25 24.6875 1.25 23.75C1.25 22.175 1.975 20.7625 3.125 19.85C3.3375 19.675 3.56247 19.525 3.79997 19.4C4.52497 18.9875 5.3625 18.75 6.25 18.75C7.5 18.75 8.62497 19.2 9.49997 19.9625C9.64997 20.075 9.78749 20.2125 9.91249 20.35C10.7375 21.25 11.25 22.4375 11.25 23.75Z" stroke="#898989" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M8.11267 23.7246H4.3877" stroke="#898989" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M6.25 21.8999V25.6374" stroke="#898989" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                                           
                    <span>Loket Tiket</span>
                </a>
            </li>
        </ul>
    </aside>
</div>