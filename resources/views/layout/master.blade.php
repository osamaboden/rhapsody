<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- <link rel="apple-touch-icon" sizes="180x180" href="{{  asset('assets/icon/apple-touch-icon.png') }}"> --}}
    {{-- <link rel="icon" type="image/png" sizes="32x32" href="{{  asset('assets/icon/favicon-32x32.png') }}"> --}}
    {{-- <link rel="icon" type="image/png" sizes="16x16" href="{{  asset('assets/icon/favicon-16x16.png') }}"> --}}
    {{-- <link rel="manifest" href="{{  asset('assets/icon/site.webmanifest') }}"> --}}
    {{-- <link rel="mask-icon" href="{{  asset('assets/icon/safari-pinned-tab.svg') }}" color="#5bbad5"> --}}
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <title>
        @if (isset($title))
        {{ $title }}
        @else
        @hasSection('title')
        @yield('title')
        @else
        Corteva
        @endif
        @endif
    </title>

    <!-- General CSS Files -->
    <link rel="stylesheet" href="{{ asset('assets/modules/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/fontawesome/css/all.min.css') }}">

    <!-- CSS Libraries -->
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/datatables.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('assets/modules/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/datatables/Select-1.2.4/css/select.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/modules/select2/dist/css/select2.min.css') }}">


    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/components.css') }}">
    <!-- Custom css -->

    <link rel="stylesheet" href="{{ asset('assets/css/main/main.css') }}">
    <!-- modal -->
    <link rel="stylesheet" href="{{ asset('assets/css/component/component.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugin/timepicker/timepicker.min.css')}}">
    @if(!request()->routeIs(['re-registration'])) 
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    @endif

    @stack('css')
</head>

<body>
    @if(request()->routeIs(['queue.dash', 'nominate.index', 'booth.public']))
    <div id="app" class="queue-container">
    @else
    <div id="app">
    @endif
        <div class="main-wrapper main-wrapper-1">
            @if(request()->routeIs(['nominate.display','queue','login', 'setup-password', 'template-event-1', 'event', 'registration.success', 'redemp.tiket','checkout.tiket', 'checkin', 're-registration.crew', 'setting.preview', 'home']))
            @yield('content')

            @else 
            @include('layout.navbar')

            @include('layout.sidebar')

            <!-- Main Content -->
            <div class="main-content">
                <section class="section">
                    @yield('content')
                </section>
            </div>
            @include('layout.footer')
            @endif
        </div>
    </div>
    @stack('modal')
    <!-- General JS Scripts -->
    @if(!request()->routeIs(['re-registration'])) 
    <script src="//cdn.ckeditor.com/4.21.0/full/ckeditor.js"></script>
    @endif
    <script src="{{ asset('assets/modules/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/modules/popper.js') }}"></script>
    <script src="{{ asset('assets/modules/tooltip.js') }}"></script>
    <script src="{{ asset('assets/modules/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/modules/nicescroll/jquery.nicescroll.min.js') }}"></script>
    <script src="{{ asset('assets/modules/moment.min.js') }}"></script>
    <script src="{{ asset('assets/js/stisla.js') }}"></script>
    <!-- Template JS File -->
    <script src="{{ asset('assets/js/scripts.js') }}"></script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>
    <script src="{{ asset('assets/modules/datatables/datatables.min.j') }}s"></script>

    <script src="{{ asset('assets/modules/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/modules/datatables/Select-1.2.4/js/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('assets/modules/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/modules/select2/dist/js/select2.full.min.js') }}"></script>

    <script src="{{ asset('assets/js/sweetaler2.js') }}"></script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script> --}}
    <script src="{{ asset('assets/js/jquery.mask.min.js') }}"></script>
    <script src="{{ asset('assets/plugin/timepicker/timepicker.min.js') }}"></script>
    <!-- Custom Js File -->

   

    @stack('javascript')
    <script src="{{ asset('assets/js/main.js') }}"></script>
</body>

</html>