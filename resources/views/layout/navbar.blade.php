<div class="navbar-bg"></div>
            <nav class="navbar navbar-expand-lg main-navbar">
                <img src="{{ asset('assets/img/logo-corteva.svg') }}" alt="logo" id="logo-navbar">
                <form class="form-inline mr-auto" style="margin-bottom: 0px;">
                    <ul class="navbar-nav mr-3">
                        <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
                    </ul>
                </form>

                <ul class="navbar-nav navbar-right">
                 
                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                            <img alt="image" src="{{ asset('assets/img/avatar/avatar-1.png') }}" class="rounded-circle mr-1">
                          
                            <div class="d-sm-none d-lg-inline-block">{{ auth('web')->user()->name ?? '' }}</div>                                
                            
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="{{route('aksilogout')}}" class="dropdown-item has-icon text-danger">
                                <i class="fas fa-sign-out-alt"></i> Logout
                            </a>
                        </div>
                    </li>
                   
                </ul>
            </nav>