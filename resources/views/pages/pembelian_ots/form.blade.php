@php
$title = $isEdit ? 'Detail Destinasi Wisata' : 'Tambah Destinasi Wisata'
@endphp
@extends('layout.master')
@section('title', $title)

@push('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css"
    integrity="sha512-+EoPw+Fiwh6eSeRK7zwIKG2MA8i3rV/DGa3tdttQGgWyatG/SkncT53KHQaS5Jh9MNOT3dmFL0FjTY08And/Cw=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
<style>
    .select2-container--default .select2-results__option[aria-disabled=true] {
        cursor: no-drop;
    }

    .wrapper-location {
        border: 1px solid #ced4da;
        border-color: #e4e6fc;
        padding: 10px 15px;
        border-radius: 0.25rem;
    }

    .mapouter {
        position: relative;
        text-align: right;
        width: 100%;
        height: 400px;
    }

    .gmap_canvas {
        overflow: hidden;
        background: none !important;
        width: 100%;
        height: 400px;
    }

    #gmap {
        width: 100% !important;
        height: 400px !important;
    }

    .item-image img {
        width: 100%;
        object-fit: contain;
        aspect-ratio: 3/2;
    }

    .item-image-delete {
        position: absolute;
        background: red;
        color: white;
        padding: 5px 10px;
        border-radius: 50%;
        top: 10px;
        right: 25px;
        cursor: pointer;
    }

    .boxarea {
        padding: 20px;
        text-align: center;
        height: 150px;
        border: 1px solid #ECEFF1;
        width: 100%;
    }

    #preview {
        width: 200px;
        height: 100%;
        object-fit: contain;
    }
</style>
@endpush

@section('content')
<form action="{{ route('destinasi.store') }}" method="POST" id="formSubmit" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="id" @if($isEdit) value="{{ $data->id }}" @endif>
    <div class="row mt-3">
        <div class="col-8">
            <div class="title">
                {{ $title }}
            </div>
            <nav aria-label="breadcrumb" class="mt-2">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('destinasi.index') }}">Destinasi Wisata</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">{{ $isEdit ? 'Detail' : 'Tambah' }}</li>
                </ol>
            </nav>

        </div>
        <div class="col-4 d-flex align-items-center justify-content-end">
            <button target="_blank" type="submit" class="btn btn-blue">Simpan</button>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-12" id="alertMessage"></div>
        <div class="col-12 col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="total-item">
                        Data Destinasi Wisata
                    </div>
                    <div class="row mt-4">
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <label>Nama</label>
                                <input type="text" class="form-control" name="nama" placeholder="Masukkan nama lokasi"
                                    @if($isEdit) value="{{ $data->nama }}" @endif required>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <label>Kategori</label>
                                <select name="kategori" id="kategori" data-placeholder="Daftar Kategori" class="select2"
                                    required>
                                    <option></option>
                                    <option value="wisata" @selected($isEdit && $data->kategori == "wisata")>Wisata</option>
                                    <option value="kuliner" @selected($isEdit && $data->kategori == "kuliner")>Kuliner</option>
                                    <option value="oleh-oleh" @selected($isEdit && $data->kategori == "oleh-oleh")>Oleh-oleh</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <label>Foto</label>
                                <div class="boxarea" id="uploadFile">
                                    <img id="preview" @if($isEdit) src="{{ asset('storage/files/'.$data->foto) }}" data-default="{{ asset('storage/files/'.$data->foto) }}" @else src="{{ asset('assets/img/gallery-add.svg') }}" data-default="{{ asset('assets/img/gallery-add.svg') }}" @endif alt="your image" />
                                </div>
                            </div>
                            <input accept="image/*" hidden type='file' name="file" id="file" />
                        </div>
                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label>Deskripi</label>
                                <textarea type="text" class="form-control" id="deskripsi" name="deskripsi"
                                    placeholder="Masukkan deskripsi lokasi"
                                    required>@if($isEdit) {{ $data->deskripsi }} @endif</textarea>
                            </div>
                        </div>

                        <div class="col-12 col-sm-12">
                            <div class="form-group">
                                <label>Alamat</label>
                                <textarea type="text" class="form-control" id="address" name="alamat"
                                    placeholder="Masukkan alamat lokasi"
                                    required>@if($isEdit) {{ $data->alamat }} @endif</textarea>
                                <div class="wrapper-location mt-2">
                                    <div class="mapouter ">
                                        <div class="gmap_canvas">
                                            <div id="gmap"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-danger mt-2">*) Geser pin jika posisi belum sesuai</div>

                                <input type="hidden" name="koordinat" id="koordinate" @if($isEdit)
                                    value="{{ $data->koordinat }}" @endif>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

@endsection

@push('javascript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"
    integrity="sha512-IsNh5E3eYy3tr/JiX2Yx4vsCujtkhwl7SLqgnwLNgf04Hrt9BT9SXlLlZlWx+OK4ndzAoALhsMNcCmkggjZB1w=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script async src="https://maps.googleapis.com/maps/api/js?key={{ env('MAPS_API') }}&loading=async&callback=initMap">
</script>
<script>
    const initDom = () => {
        $('.number_mask').mask('0000000000000');
        $('.item-image-delete').click(function(){
            var el = $(this)
            if (el.attr('data-target')) {
                var row = el.attr('data-target');
                $(`${row}`).remove();
            }  
        })
    }

    const setMessage = (target = "#alertMessage", pesan = '') => {
        var element = $(target)
        element.html('')
        if (pesan) {
            element.html(`<div class="alert alert-danger">${pesan}</div>`)
            setTimeout(() => {
                element.html('')
            }, 5000);
        }
    }

    const randomString = (length) => {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        let counter = 0;
        while (counter < length) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
            counter += 1;
        }
        return result;
    }


    $(document).ready(function(){
        initDom();

        $('.popup-image').magnificPopup({
            type: 'image'
        });

    const editor = CKEDITOR.replace('deskripsi', {
        height: 250,
        toolbar: [{
                name: 'styles',
                items: ['Styles', 'Format', 'Font', 'FontSize']
            },
            {
                name: 'basicstyles',
                groups: ['basicstyles', 'cleanup'],
                items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript']
            },
            {
                name: 'paragraph',
                groups: ['list', 'indent', 'blocks', 'align', 'bidi'],
                items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-',
                    'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock',
                ]
            },
            {
                name: 'clipboard',
                groups: ['clipboard', 'undo'],
                items: ['Cut', 'Copy', 'Paste', '-', 'Undo', 'Redo']
            },
        ],
    });

    $('#uploadFile').click(function(){
        $('#file').val('')
        $('#file').click();
    })

$('#file').change(function (e) {
    var files = e.target.files;
    $('#error_logo').text('');
    if (files.length > 0) {
        var maxSize = 2 * 1024 * 1024;
        var size = files[0].size;
        if (maxSize < size) {
            $('#file').val('')
            setMessage("#alertMessage", "Ukuran gambar maximal 2MB")
        } else {
            $('#preview').attr('src', URL.createObjectURL(files[0]))
        }
    } else {
        $('#preview').attr('src', $('#preview').attr("data-default"))
        $('#file').val('');
    }
})

})

    $('#formSubmit').on("submit", function(e) {
        e.preventDefault();
        var element = $(this)
        var formData = new FormData(this);

        formData.append('deskripsi', CKEDITOR.instances.deskripsi.getData());

        var buttonSubmit = element.find('[type="submit"]');
        $.ajax({
            url: element.attr("action"),
            type: "POST",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function(){
                buttonSubmit.attr('disabled', true)
                buttonSubmit.html('<i class="fas fa-spinner fa-spin"></i>')
            },
            success: function(res) {
                buttonSubmit.attr('disabled', false)
                buttonSubmit.html('Simpan')
                if(res.is_success) {
                    location.href = res.route
                } else {
                    setMessage("#alertMessage", "Gagal menyimpan data, silakan ulangi kembali nanti")
                }
            },
            
            error: function() {
                setMessage("#alertMessage", "Gagal menyimpan data, silakan ulangi kembali nanti");
                buttonSubmit.attr('disabled', false);
                buttonSubmit.html('Simpan')
            }
        })
    })
</script>

<script>
    var defaultLat = {
        lat: -6.175355010907905,
        lng: 106.82715894657615
    };
    if ($('#koordinate').val()) {
        var split =  $('#koordinate').val().split(",");
        defaultLat = {
            lat: parseFloat(split[0]),
            lng: parseFloat(split[1])
        }
    }
    var map, marker, geocoder;
    async function initMap() {
        const { Map } = await google.maps.importLibrary("maps");

        map = new Map(document.getElementById("gmap"), {
            center: { lat: defaultLat.lat, lng: defaultLat.lng },
            zoom: 14,
            mapTypeControl: false,
            streetViewControl: false,
            fullscreenControl: false,
        });

        marker = new google.maps.Marker({
            position: { lat: defaultLat.lat, lng: defaultLat.lng },
            map,
            draggable:true,
            title: "Lokasi",
        });

        marker.addListener('dragend', (e) => {
            $('#koordinate').val(e.latLng.lat()+","+e.latLng.lng());
            map.setCenter(e.latLng);
        });

        geocoder = new google.maps.Geocoder();
    }

    getCoordinateFromAddress = () => {
        var address = $('#address').val()
        if (address) {
            geocoder.geocode( { 'address': address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();
                    map.setCenter(results[0].geometry.location)
                    marker.setPosition(results[0].geometry.location)
                    $('#koordinate').val(latitude+","+longitude);

                } 
            });
        }
    }

    $("#tampilkanDipeta").click(getCoordinateFromAddress)

    $('#address').change(getCoordinateFromAddress)
</script>
@endpush