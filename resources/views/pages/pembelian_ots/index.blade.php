@extends('layout.master')
@push("css")
<style>
    .modal-dialog {
        max-width: 700px !important;
    }

    .logo {
        width: 140px;
        aspect-ratio: 3/2;
        object-fit: contain;
    }

    .tribun {
        width: 100%;
        color: #fff;
        font-weight: 500;
        padding: 15px;
        border-radius: 5px;
        text-align: center;
    }

    #preview {
        width: 200px;
        height: 100%;
        object-fit: contain;
    }

    .boxarea {
        padding: 20px;
        text-align: center;
        height: 150px;
        border: 1px solid #ECEFF1;
        width: 100%;
    }

    .btn-file {
        width: 100%;
        font-weight: 500;
        border: 0px;
        border-radius: 5px;
        color: #898989;
        background: #ECEFF1;
        padding: 15px;
    }

    .nav-pills .nav-item .nav-link.active {
        /* box-shadow: 0 2px 6px #0cadf5; */
        color: #fff;
        background-color: #159cd8;
    }
    .select2-container--default .select2-selection--single .select2-selection__clear::before {
        content: "x";
        z-index: 4;
        position: absolute;
        color: white;
        left: 6px;
        bottom: 2px;
        font-size: 13px;
    }
    .select2-container--default .select2-selection--single .select2-selection__clear::after {
        content: "";
        position: absolute;
        width: 20px;
        height: 20px;
        background: red;
        color: white;
        border-radius: 50%;
        text-align: center;
        top: 10px;
        left: 0;
        z-index: 1;
    }

    #modalScan .modal-dialog {
        max-width: 600px !important;
    }

    #modalScan .modal-content {
        background-color: transparent;
        box-shadow: unset !important;
    }

    #modalScan #cameraPreview {
        border-radius: 10px;
    }
</style>
<link rel="stylesheet" href="{{ asset('assets/css/participant/participant.css') }}">
@endpush
@section('title', 'Loket Tiket')

@section('content')
<div class="row mt-3">
    <div class="col-12 d-flex justify-content-between align-items-center">
        <div class="title">
            Loket Tiket
        </div>
        <div class="d-flex align-items-center">
            <button type="button" id="btnScan" class="btn btn-green pb-2 pt-2 mr-2">Gelang Keluar</button>
            {{-- <a href="{{ route('re-registration') }}" target="_blank" type="button"
                class="btn btn-save pb-2 pt-2">Checkin Peserta Raker</a> --}}
        </div>
    </div>
</div>

<div class="mt-3">
    <form action="{{ route('pembelian_ots.store') }}" method="POST" id="formSubmit">
        @csrf
        <div class="card">
            <div class="card-body">
                <div class="d-flex align-items-center justify-content-between">
                    <div class="total-item">
                        Form Pembelian / Penukaran Tiket
                    </div>
                    <div class="">
                        <button type="submit" class="btn btn-blue">Simpan</button>
                    </div>
                </div>
                <div id="alertMessage"></div>
                <div class="row mt-3">
                    <div class="col-12 col-sm-4">
                        <div class="form-group mb-2">
                            <label>Nama <span class="text-danger">*</span></label>
                            <input type="text" name="nama" id="nama" class="form-control"
                                placeholder="Masukkan nama anda" required>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group mb-2">
                            <label>No. HP</label>
                            <input type="text" name="no_hp" id="no_hp" class="form-control number_mask"
                                placeholder="Masukkan No. HP anda">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group mb-2">
                            <label>Metode Pembayaran <span class="text-danger">*</span></label>
                            <select type="text" name="metode_bayar" id="metode_bayar" class="form-control"
                                data-placeholder="Masukkan/Ketik Metode Pembayaran" required>
                                <option></option>
                                @foreach ($metodeBayar as $item)
                                <option value="{{ $item }}">{{ $item }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group mb-2">
                            <label>Kode Booking</label>
                            <input type="text" name="kode_booking" id="kode_booking" class="form-control" placeholder="Masukkan kode booking">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group mb-2">
                            <label>Jumlah Tiket</label>
                            <input type="text" name="jumlah_tiket" id="jumlah_tiket" value="1"
                                class="form-control number_mask" placeholder="Masukkan jumlah tiket (default 1)">
                        </div>
                    </div>
                    <div class="col-12 col-sm-4">
                        <div class="form-group mb-2">
                            <label>Email</label>
                            <input type="email" name="email" id="email" class="form-control"
                                placeholder="Masukkan email anda">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<div class="mt-3">
    <div class="card">
        <div class="card-body">
            <div class="d-flex justify-content-between align-items-center">
                <div class="total-item container-count">
                    Daftar Data
                </div>
                <div class="d-flex">
                    <div class="mr-2">
                        <input type="text" placeholder="search" id="search" class="form-control">
                    </div>
                    <div class="mr-2">
                        <a href="{{ route('pembelian_ots.export') }}" data-href="{{ route('pembelian_ots.export') }}"
                            id="btnExport" target="_blank" class="btn btn-orange"><svg width="16" height="16"
                                viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M14 10V12.6667C14 13.0203 13.8595 13.3594 13.6095 13.6095C13.3594 13.8595 13.0203 14 12.6667 14H3.33333C2.97971 14 2.64057 13.8595 2.39052 13.6095C2.14048 13.3594 2 13.0203 2 12.6667V10"
                                    stroke="white" stroke-width="1.33333" stroke-linecap="round"
                                    stroke-linejoin="round" />
                                <path d="M11.3337 5.33333L8.00033 2L4.66699 5.33333" stroke="white"
                                    stroke-width="1.33333" stroke-linecap="round" stroke-linejoin="round" />
                                <path d="M8 2V10" stroke="white" stroke-width="1.33333" stroke-linecap="round"
                                    stroke-linejoin="round" />
                            </svg>
                            Export</a>
                    </div>
                </div>
            </div>
            <div class="mt-3">
                <table class="table table-striped" id="tableDestinasi">
                    <thead>
                        <tr class="text-center">
                            <th>Nama</th>
                            <th>Email</th>
                            <th>No. HP</th>
                            <th>Metode Bayar</th>
                            <th>Kode Booking</th>
                            <th>Jumlah</th>
                            <th>Waktu</th>
                            <th>Loket</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@push('modal')
<div class="modal fade" id="modalScan" role="dialog" action="{{ route('re-registration.check') }}" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body p-0 position-relative">
                <div id="removeButton" style="position: absolute; color: black;right: 13px;top: 6px; cursor: pointer; z-index: 1"><i
                        class="fas fa-times" style="font-size: 35px;"></i></div>
                <video style="width: 100%" id="cameraPreview"></video>
            </div>
        </div>
    </div>
</div>
@endpush

@push('javascript')
<script type="text/javascript" src="{{ asset('assets/js/zxing-qr.min.js') }}"></script>
<script src="{{ asset('assets/js/rhapsody.js?v='.uniqid()) }}" type="module"></script>
<script>
    var table = null;
    const setTable = () => {
        if (table) {
            table.destroy();
            table = '';
        }
        table = $("#tableDestinasi").DataTable({
            processing: true,
            serverSide: true,
            ajax: location.href,
            autoWidth: false,
            lengthChange: false,
            orerin: [],
            ordering: false,
            searching: false,
            pageLength: 25,
            columns: [
                {data: 'nama', name: 'nama'},
                {data: 'email', name: 'email'},
                {data: 'no_hp', name: 'no_hp'},
                {data: 'metode_bayar', name: 'metode_bayar'},
                {data: 'kode_booking', name: 'kode_booking'},
                {data: 'jumlah_tiket', name: 'jumlah_tiket', className: 'text-center'},
                {data: 'created_at', name: 'created_at'},
                {data: 'loket', name: 'loket', className: 'text-center'},
                // {data: 'status', name: 'status'},
            ],
            initComplete: function(setting, json) {
                // $('.container-count').text(`${formatMoney(json.recordsTotal)} Pembelian`)
            }
        });
    }

    const formatMoney = (a) => {
        if (!a) {
            return '0';
        }

        return a.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
    }

    const setMessage = (target = '#alertMessage', pesan = '', className = 'alert alert-danger mt-3') => {
        var element = $(target)
        element.html('')
        if (pesan) {
            element.html(`<div class="${className}">${pesan}</div>`)
            setTimeout(() => {
                element.html('')
            }, 5000);
        }
    }

    $(document).ready(function () {
        setTable();
        $('.number_mask').mask("000000000000")
        $('#metode_bayar').select2({
            tags: true,
            allowClear: true,
        })

        $('#jumlah_tiket').change(function(){
            var val = $(this).val()
            if(!val) {
                $(this).val(1)
            }
        })
    })

    $('body').on('click', '[data-toggle="showModal"]', function(e) {
        var route = $(this).attr('data-route')
        if (route) {
            window.location.href = route;
        }
    })

    $('#formSubmit').on("submit", function(e) {
        e.preventDefault();
        var element = $(this)
        var formData = new FormData(this);
        var buttonSubmit = element.find('[type="submit"]');
        $.ajax({
            url: element.attr("action"),
            type: "POST",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            pageLength: 20,
            beforeSend: function(){
                buttonSubmit.attr('disabled', true)
                buttonSubmit.html('<i class="fas fa-spinner fa-spin"></i>')
            },
            success: function(res) {
                buttonSubmit.attr('disabled', false)
                buttonSubmit.html('Simpan')
                if(res.is_success) {
                    setTable();
                    $("#formSubmit").trigger('reset');
                    $('#metode_bayar').val('').trigger('change')
                    setMessage("#alertMessage", "Data berhasil disimpan", "alert alert-success mt-3")
                } else {
                    setMessage("#alertMessage", "Gagal menyimpan data, silakan ulangi kembali nanti")
                }
            },
            
            error: function() {
                setMessage("#alertMessage", "Gagal menyimpan data, silakan ulangi kembali nanti");
                buttonSubmit.attr('disabled', false);
                buttonSubmit.html('Simpan')
            }
        })
    })
</script>
@endpush('javascript')