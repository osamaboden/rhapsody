@extends('layout.master')
@push('css')
<link rel="stylesheet" href="{{ asset('assets/css/registration/registration.css') }}">
@endpush
@section('content')
<nav class="navbar navbar-wrapper navbar-expand-lg scrolling-active">
    <div class="container">
        <a class="navbar-brand" href="/"> <img src="{{ asset('storage/files/'.$default['logo']) }}" alt="" class=""></a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-center py-3" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item mx-2">
                    <a class="nav-link" href="{{ route('event') }}#section-home">Home</a>
                </li>
                <li class="nav-item mx-2">
                    <a class="nav-link" href="{{ route('event') }}#section-lineup">Line Up</a>
                </li>
                <li class="nav-item mx-2">
                    <a class="nav-link" href="{{ route('event') }}#section-registration">Registration</a>
                </li>
                <li class="nav-item mx-2">
                    <a class="nav-link" href="{{ route('event') }}#section-trailer">About Us</a>
                </li>
            </ul>
        </div>
        <a href="tel:{{ $default['contact_phone'] }}" target="_blank" class="btn btn-contact">
            Contact
            <svg width="19" height="18" viewBox="0 0 19 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                <g clip-path="url(#clip0_19_13039)">
                    <path
                        d="M16.6131 12.747V14.9728C16.614 15.1794 16.5716 15.3839 16.4889 15.5733C16.4061 15.7626 16.2847 15.9325 16.1324 16.0722C15.9802 16.2119 15.8004 16.3183 15.6047 16.3845C15.4089 16.4506 15.2015 16.4752 14.9957 16.4566C12.7127 16.2086 10.5197 15.4284 8.59293 14.1789C6.80032 13.0398 5.28049 11.52 4.14139 9.7274C2.88753 7.79187 2.10722 5.5882 1.86369 3.29492C1.84515 3.08976 1.86953 2.88298 1.93528 2.68775C2.00104 2.49253 2.10672 2.31313 2.2456 2.16098C2.38449 2.00884 2.55353 1.88728 2.74197 1.80405C2.93041 1.72081 3.13411 1.67773 3.34012 1.67753H5.56588C5.92594 1.67399 6.27501 1.80149 6.54801 2.03628C6.82102 2.27106 6.99934 2.59711 7.04973 2.95364C7.14367 3.66593 7.3179 4.36532 7.56908 5.03844C7.6689 5.30399 7.6905 5.5926 7.63133 5.87005C7.57216 6.1475 7.43469 6.40218 7.23521 6.6039L6.29297 7.54614C7.34914 9.40358 8.88707 10.9415 10.7445 11.9977L11.6867 11.0554C11.8885 10.856 12.1431 10.7185 12.4206 10.6593C12.6981 10.6001 12.9867 10.6218 13.2522 10.7216C13.9253 10.9728 14.6247 11.147 15.337 11.2409C15.6974 11.2918 16.0266 11.4733 16.2618 11.751C16.4971 12.0287 16.6221 12.3832 16.6131 12.747Z"
                        stroke="white" stroke-width="1.48385" stroke-linecap="round" stroke-linejoin="round" />
                </g>
                <defs>
                    <clipPath id="clip0_19_13039">
                        <rect width="17.8062" height="17.8062" fill="white" transform="translate(0.290771 0.193695)" />
                    </clipPath>
                </defs>
            </svg>
        </a>
    </div>
</nav>
<section class="registration-success">
    <div class="container">
        <div class="icon text-center"> <img src="{{asset ('assets/img/home/success-regist.png')}}" alt=""></div>
        <h5 class="title text-center">Congratulations, You Have <span>Successfully</span> Registered</h5>
        <div class="mt-3 text-center" style="ont-size: 16px;">Ticket sent to your email. Please check your email.</div>
        <div class="row justify-content-center mt-5">
            <div class="col-md-7">
                <div class="data-user">
                    <h5 class="text-center">Your Data</h5>
                    <div class="row align-items-center mt-3 justify-content-center">
                        <div class="col-4" style="display: flex;justify-content: center;flex-wrap: inherit;">
                            <div class="barcode" style="width: 80%;">
                                <img src="{{asset ('storage/qr_code/'.$data->uniq_code.'.png')}}" alt="">
                            </div>
                            <div class="code text-center">{{ $data->uniq_code }}</div>
                        </div>
                        <div class="data col-7">
                            <div class="d-flex mb-4">
                                <div class="label">
                                    Name
                                </div>
                                <div class="value">
                                    {{ $data->nama }}
                                </div>
                            </div>
                            <div class="d-flex mb-4">
                                <div class="label">
                                    Phone Number
                                </div>
                                <div class="value">
                                    {{ $data->no_hp }}
                                </div>
                            </div>
                            <div class="d-flex mb-4">
                                <div class="label">
                                    Email
                                </div>
                                <div class="value">
                                    {{ $data->email }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> @endsection