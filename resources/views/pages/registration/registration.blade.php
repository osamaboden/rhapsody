@extends('layout.master')
@push('css')
<link rel="stylesheet" href="{{ asset('assets/css/re-registration/re-registration.css') }}">
<style>
    #modalScan .modal-dialog {
        max-width: 600px !important;
    }

    #modalScan .modal-content {
        background-color: transparent;
        box-shadow: unset !important;
    }

    #modalScan #cameraPreview {
        border-radius: 10px;
    }

    #btnScan {
        background: white;
        cursor: pointer;
    }

    #btnScan:hover {
        background: rgb(197, 197, 197);
    }
</style>
@endpush
@section('title', $type == 'redemp' ? 'Redemp Tiket' : 'Gelang Keluar')
@section('content')
<section class="re-registration mt-0" id="sectionRe-registration" data-type="{{ $type }}">
    {{-- <img src="{{ asset('assets/img/logo/logo.svg') }}" class="logo" alt="logo">
    <img src="{{ asset('assets/img/registrasi.png') }}" class="img-bottom" alt="logo"> --}}
    <img src="{{ asset('assets/img/logo/logo_rhapsody.png') }}" alt="logo" class="logo">
    {{-- <img src="{{ asset('assets/img/logo-corteva.svg') }}" alt="tagline" class="tagline"> --}}

    <div class="container text-center wrapper-registration">
        <form action="{{ route('re-registration.check') }}" id="formRegisTration" method="POST">
            <div class="row justify-content-center">
                <div class="col-12 mb-4">
                    <span class="tagline">{{ $type == 'redemp' ? 'GATE' : 'LOKET' }} {{ auth()->user()->loket }}</span>
                </div>
                <div class="col-lg-3 col-12">
                    <div class="d-flex justify-content-center">
                        <button id="btnScan" type="button" class="border d-flex justify-content-center align-items-center">
                            <svg width="36" height="37" viewBox="0 0 36 37" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M11.4545 33.2273H4.90909C4.4751 33.2273 4.05888 33.0549 3.75201 32.748C3.44513 32.4411 3.27273 32.0249 3.27273 31.5909V25.0455C3.27273 24.6115 3.10033 24.1952 2.79345 23.8884C2.48657 23.5815 2.07035 23.4091 1.63636 23.4091C1.20237 23.4091 0.786157 23.5815 0.47928 23.8884C0.172402 24.1952 0 24.6115 0 25.0455V31.5909C0 32.8929 0.517206 34.1415 1.43784 35.0622C2.35847 35.9828 3.60712 36.5 4.90909 36.5H11.4545C11.8885 36.5 12.3048 36.3276 12.6116 36.0207C12.9185 35.7138 13.0909 35.2976 13.0909 34.8636C13.0909 34.4296 12.9185 34.0134 12.6116 33.7066C12.3048 33.3997 11.8885 33.2273 11.4545 33.2273ZM34.3636 23.4091C33.9296 23.4091 33.5134 23.5815 33.2066 23.8884C32.8997 24.1952 32.7273 24.6115 32.7273 25.0455V31.5909C32.7273 32.0249 32.5549 32.4411 32.248 32.748C31.9411 33.0549 31.5249 33.2273 31.0909 33.2273H24.5455C24.1115 33.2273 23.6952 33.3997 23.3884 33.7066C23.0815 34.0134 22.9091 34.4296 22.9091 34.8636C22.9091 35.2976 23.0815 35.7138 23.3884 36.0207C23.6952 36.3276 24.1115 36.5 24.5455 36.5H31.0909C32.3929 36.5 33.6415 35.9828 34.5622 35.0622C35.4828 34.1415 36 32.8929 36 31.5909V25.0455C36 24.6115 35.8276 24.1952 35.5207 23.8884C35.2138 23.5815 34.7976 23.4091 34.3636 23.4091ZM31.0909 0.5H24.5455C24.1115 0.5 23.6952 0.672402 23.3884 0.97928C23.0815 1.28616 22.9091 1.70237 22.9091 2.13636C22.9091 2.57035 23.0815 2.98657 23.3884 3.29345C23.6952 3.60033 24.1115 3.77273 24.5455 3.77273H31.0909C31.5249 3.77273 31.9411 3.94513 32.248 4.25201C32.5549 4.55888 32.7273 4.9751 32.7273 5.40909V11.9545C32.7273 12.3885 32.8997 12.8048 33.2066 13.1116C33.5134 13.4185 33.9296 13.5909 34.3636 13.5909C34.7976 13.5909 35.2138 13.4185 35.5207 13.1116C35.8276 12.8048 36 12.3885 36 11.9545V5.40909C36 4.10712 35.4828 2.85847 34.5622 1.93784C33.6415 1.01721 32.3929 0.5 31.0909 0.5ZM1.63636 13.5909C2.07035 13.5909 2.48657 13.4185 2.79345 13.1116C3.10033 12.8048 3.27273 12.3885 3.27273 11.9545V5.40909C3.27273 4.9751 3.44513 4.55888 3.75201 4.25201C4.05888 3.94513 4.4751 3.77273 4.90909 3.77273H11.4545C11.8885 3.77273 12.3048 3.60033 12.6116 3.29345C12.9185 2.98657 13.0909 2.57035 13.0909 2.13636C13.0909 1.70237 12.9185 1.28616 12.6116 0.97928C12.3048 0.672402 11.8885 0.5 11.4545 0.5H4.90909C3.60712 0.5 2.35847 1.01721 1.43784 1.93784C0.517206 2.85847 0 4.10712 0 5.40909V11.9545C0 12.3885 0.172402 12.8048 0.47928 13.1116C0.786157 13.4185 1.20237 13.5909 1.63636 13.5909ZM14.7273 7.04545H8.18182C7.74783 7.04545 7.33161 7.21786 7.02473 7.52473C6.71786 7.83161 6.54545 8.24783 6.54545 8.68182V15.2273C6.54545 15.6613 6.71786 16.0775 7.02473 16.3844C7.33161 16.6912 7.74783 16.8636 8.18182 16.8636H14.7273C15.1613 16.8636 15.5775 16.6912 15.8844 16.3844C16.1912 16.0775 16.3636 15.6613 16.3636 15.2273V8.68182C16.3636 8.24783 16.1912 7.83161 15.8844 7.52473C15.5775 7.21786 15.1613 7.04545 14.7273 7.04545ZM13.0909 13.5909H9.81818V10.3182H13.0909V13.5909ZM21.2727 16.8636H27.8182C28.2522 16.8636 28.6684 16.6912 28.9753 16.3844C29.2821 16.0775 29.4545 15.6613 29.4545 15.2273V8.68182C29.4545 8.24783 29.2821 7.83161 28.9753 7.52473C28.6684 7.21786 28.2522 7.04545 27.8182 7.04545H21.2727C20.8387 7.04545 20.4225 7.21786 20.1156 7.52473C19.8088 7.83161 19.6364 8.24783 19.6364 8.68182V15.2273C19.6364 15.6613 19.8088 16.0775 20.1156 16.3844C20.4225 16.6912 20.8387 16.8636 21.2727 16.8636ZM22.9091 10.3182H26.1818V13.5909H22.9091V10.3182ZM14.7273 20.1364H8.18182C7.74783 20.1364 7.33161 20.3088 7.02473 20.6156C6.71786 20.9225 6.54545 21.3387 6.54545 21.7727V28.3182C6.54545 28.7522 6.71786 29.1684 7.02473 29.4753C7.33161 29.7821 7.74783 29.9545 8.18182 29.9545H14.7273C15.1613 29.9545 15.5775 29.7821 15.8844 29.4753C16.1912 29.1684 16.3636 28.7522 16.3636 28.3182V21.7727C16.3636 21.3387 16.1912 20.9225 15.8844 20.6156C15.5775 20.3088 15.1613 20.1364 14.7273 20.1364ZM13.0909 26.6818H9.81818V23.4091H13.0909V26.6818ZM21.2727 25.0455C21.7067 25.0455 22.1229 24.8731 22.4298 24.5662C22.7367 24.2593 22.9091 23.8431 22.9091 23.4091C23.3431 23.4091 23.7593 23.2367 24.0662 22.9298C24.3731 22.6229 24.5455 22.2067 24.5455 21.7727C24.5455 21.3387 24.3731 20.9225 24.0662 20.6156C23.7593 20.3088 23.3431 20.1364 22.9091 20.1364H21.2727C20.8387 20.1364 20.4225 20.3088 20.1156 20.6156C19.8088 20.9225 19.6364 21.3387 19.6364 21.7727V23.4091C19.6364 23.8431 19.8088 24.2593 20.1156 24.5662C20.4225 24.8731 20.8387 25.0455 21.2727 25.0455ZM27.8182 20.1364C27.3842 20.1364 26.968 20.3088 26.6611 20.6156C26.3542 20.9225 26.1818 21.3387 26.1818 21.7727V26.6818C25.7478 26.6818 25.3316 26.8542 25.0247 27.1611C24.7179 27.468 24.5455 27.8842 24.5455 28.3182C24.5455 28.7522 24.7179 29.1684 25.0247 29.4753C25.3316 29.7821 25.7478 29.9545 26.1818 29.9545H27.8182C28.2522 29.9545 28.6684 29.7821 28.9753 29.4753C29.2821 29.1684 29.4545 28.7522 29.4545 28.3182V21.7727C29.4545 21.3387 29.2821 20.9225 28.9753 20.6156C28.6684 20.3088 28.2522 20.1364 27.8182 20.1364ZM21.2727 26.6818C20.9491 26.6818 20.6327 26.7778 20.3636 26.9576C20.0945 27.1374 19.8848 27.393 19.7609 27.692C19.6371 27.991 19.6047 28.32 19.6678 28.6374C19.7309 28.9548 19.8868 29.2464 20.1156 29.4753C20.3445 29.7041 20.6361 29.86 20.9535 29.9231C21.2709 29.9862 21.5999 29.9538 21.8989 29.83C22.1979 29.7061 22.4535 29.4964 22.6333 29.2273C22.8131 28.9582 22.9091 28.6418 22.9091 28.3182C22.9091 27.8842 22.7367 27.468 22.4298 27.1611C22.1229 26.8542 21.7067 26.6818 21.2727 26.6818Z"
                                    fill="#2B2F33" />
                            </svg>
                            <span class="ml-3">Scan QR Code</span>
                        </button>
                        {{-- <div class="line"></div> --}}
                    </div>

                </div>

                {{-- <div class="col-lg-4 col">
                    <input type="text" class="form-control text-uppercase number" id="nomorHp" placeholder="Masukkan No HP" required>
                </div> --}}
                <div class="col-lg-9 col-12">
                    <input type="text" class="form-control text-uppercase" id="uniqCode"
                        placeholder="Masukkan Kode Unik" required>
                        <input type="hidden" name="type" id="type" value="{{ $type }}">
                        <div id="btn-regist">
                            <button type="sumbit" class="btn">
                                <svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M16.5 28.5C23.1274 28.5 28.5 23.1274 28.5 16.5C28.5 9.87258 23.1274 4.5 16.5 4.5C9.87258 4.5 4.5 9.87258 4.5 16.5C4.5 23.1274 9.87258 28.5 16.5 28.5Z"
                                        stroke="white" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" />
                                    <path d="M31.5016 31.5001L24.9766 24.9751" stroke="white" stroke-width="3"
                                        stroke-linecap="round" stroke-linejoin="round" />
                                </svg>
                            </button>
                        </div>
                </div>

            </div>
        </form>
    </div>
    {{-- <img src="{{asset ('assets/img/img-banner-1.png')}}" alt="" class="img-banner"> --}}
</section>

@endsection

@push('modal')
<div class="modal fade" id="modalScan" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body p-0 position-relative">
                <div id="removeButton" style="position: absolute; color: black;right: 13px;top: 6px; cursor: pointer; z-index: 1"><i
                        class="fas fa-times" style="font-size: 35px;"></i></div>
                <video style="width: 100%" id="cameraPreview"></video>
            </div>
        </div>
    </div>
</div>
@endpush
@push("javascript")
<script type="text/javascript" src="{{ asset('assets/js/zxing-qr.min.js') }}"></script>
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/html5-qrcode/2.3.7/html5-qrcode.min.js" integrity="sha512-dH3HFLqbLJ4o/3CFGVkn1lrppE300cfrUiD2vzggDAtJKiCClLKjJEa7wBcx7Czu04Xzgf3jMRvSwjVTYtzxEA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> --}}
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"
    integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script> --}}
<script src="{{ asset('assets/js/rhapsody.js?v='.uniqid()) }}" type="module"></script>
@endpush