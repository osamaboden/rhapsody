@extends('layout.master')
@push("css")
<style>
    .modal-dialog {
        max-width: 700px !important;
    }

    #preview {
        height: 100%;
        object-fit: contain;
    }

    .btn-file {
        width: 100%;
        font-weight: 500;
        border: 0px;
        border-radius: 5px;
        color: #898989;
        background: #ECEFF1;
        padding: 9px;
    }

    .boxarea img {
        width: 100%;
        height: 100%;
        aspect-ratio: 3/2;
        object-fit: contain;
    }

    #alamatText {
        width: 500px;
    }

    .edit-address {
        cursor: pointer;
        border-radius: 10px;
        padding: 10px 20px;
        background: #FCE5C3;
        color: #FFA826;
    }

    .form-info {
        padding-top: 5px;
        font-size: 14px;
        font-weight: normal;
    }
</style>
<link rel="stylesheet" href="{{ asset('assets/css/dashboard/dashboard.css') }}">
@endpush
@section('title', 'Dashboard')

@section('content')
<div class="row mt-3">
    <div class="col-12 d-flex justify-content-between align-items-center">
        <div class="title">
            DASHBOARD
        </div>
        <div class="d-flex align-items-center">
            <a href="{{ route('checkout.tiket') }}" target="_blank" type="button"
                class="btn btn-blue pb-2 pt-2 mr-2">Gelang Keluar</a>
            <a href="{{ route('redemp.tiket') }}" target="_blank" type="button"
                class="btn btn-green pb-2 pt-2">Redemp Tiket</a>
            {{-- <a href="{{ route('re-registration') }}" target="_blank" type="button"
                class="btn btn-save pb-2 pt-2">Checkin Peserta Raker</a> --}}
        </div>
    </div>
</div>

<div class="row mt-3 dashboard">
    <div class="col-12">
        <div class="row">
            <div class="col-12 col-sm-4">

                <div class="card">
                    <div class="card-body">
                        <div class="d-flex align-items-center justify-content-between">
                            <div class=" d-flex align-items-center">
                                <svg width="44" height="42" viewBox="0 0 44 42" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <rect width="44" height="42" rx="5" fill="#159CD8" />
                                    <path
                                        d="M29.7915 33.8957H15.2082C10.8332 33.8957 7.9165 31.7082 7.9165 26.604V16.3957C7.9165 11.2915 10.8332 9.104 15.2082 9.104H29.7915C34.1665 9.104 37.0832 11.2915 37.0832 16.3957V26.604C37.0832 31.7082 34.1665 33.8957 29.7915 33.8957Z"
                                        stroke="white" stroke-width="2.5" stroke-miterlimit="10" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M13.75 15.6665V27.3332" stroke="white" stroke-width="2.5"
                                        stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round" />
                                    <path d="M18.125 15.6665V21.4998" stroke="white" stroke-width="2.5"
                                        stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round" />
                                    <path d="M18.125 25.875V27.3333" stroke="white" stroke-width="2.5"
                                        stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round" />
                                    <path d="M26.875 15.6665V17.1248" stroke="white" stroke-width="2.5"
                                        stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round" />
                                    <path d="M22.5 15.6665V27.3332" stroke="white" stroke-width="2.5"
                                        stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round" />
                                    <path d="M26.875 21.5V27.3333" stroke="white" stroke-width="2.5"
                                        stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round" />
                                    <path d="M31.25 15.6665V27.3332" stroke="white" stroke-width="2.5"
                                        stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round" />
                                </svg>




                                <div class="title-card ml-3">
                                    Jumlah QR Keluar
                                </div>

                            </div>
                            <div class="total">
                                {{ formatMoney($tiketOut) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-4">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex align-items-center justify-content-between">
                            <div class=" d-flex align-items-center">
                                <svg width="44" height="42" viewBox="0 0 44 42" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <rect width="44" height="42" rx="5" fill="#248CF0" />
                                    <path
                                        d="M29.7915 34.8957H15.2082C10.8332 34.8957 7.9165 32.7082 7.9165 27.604V17.3957C7.9165 12.2915 10.8332 10.104 15.2082 10.104H29.7915C34.1665 10.104 37.0832 12.2915 37.0832 17.3957V27.604C37.0832 32.7082 34.1665 34.8957 29.7915 34.8957Z"
                                        stroke="white" stroke-width="2.5" stroke-miterlimit="10" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path opacity="0.4" d="M13.75 16.6665V28.3332" stroke="white" stroke-width="2.5"
                                        stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round" />
                                    <path d="M18.125 16.6665V22.4998" stroke="white" stroke-width="2.5"
                                        stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round" />
                                    <path d="M18.125 26.875V28.3333" stroke="white" stroke-width="2.5"
                                        stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round" />
                                    <path d="M26.875 16.6665V18.1248" stroke="white" stroke-width="2.5"
                                        stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round" />
                                    <path opacity="0.4" d="M22.5 16.6665V28.3332" stroke="white" stroke-width="2.5"
                                        stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round" />
                                    <path d="M26.875 22.5V28.3333" stroke="white" stroke-width="2.5"
                                        stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round" />
                                    <path opacity="0.4" d="M31.25 16.6665V28.3332" stroke="white" stroke-width="2.5"
                                        stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round" />
                                </svg>

                                <div class="title-card ml-3">
                                    Jumlah QR Redemp
                                </div>

                            </div>
                            <div class="total">
                                {{ formatMoney($tiketRedemp) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-4">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex align-items-center justify-content-between">
                            <div class=" d-flex align-items-center">
                                <svg width="44" height="42" viewBox="0 0 44 42" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <rect width="44" height="42" rx="5" fill="#45D42E" />
                                    <path
                                        d="M35.6397 20.3623V26.9102C35.6397 33.4581 33.0292 36.0832 26.4667 36.0832H18.6063C17.7605 36.0832 16.9875 36.0395 16.2729 35.9374"
                                        stroke="white" stroke-width="2.5" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M9.43359 26.6332V20.3623" stroke="white" stroke-width="2.5"
                                        stroke-linecap="round" stroke-linejoin="round" />
                                    <path
                                        d="M22.5438 21.4998C25.2125 21.4998 27.1813 19.327 26.9188 16.6582L25.9416 6.9165H19.1313L18.1542 16.6582C17.8917 19.327 19.875 21.4998 22.5438 21.4998Z"
                                        stroke="white" stroke-width="2.5" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path
                                        d="M31.7312 21.4998C34.6771 21.4998 36.8354 19.1081 36.5437 16.1769L36.1353 12.1665C35.6103 8.3748 34.152 6.9165 30.3312 6.9165H25.8833L26.9042 17.1394C27.1667 19.5457 29.325 21.4998 31.7312 21.4998Z"
                                        stroke="white" stroke-width="2.5" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path
                                        d="M13.2684 21.4998C15.6746 21.4998 17.8475 19.5457 18.0809 17.1394L18.4017 13.9166L19.1017 6.9165H14.6538C10.833 6.9165 9.37466 8.3748 8.84966 12.1665L8.44129 16.1769C8.14962 19.1081 10.3225 21.4998 13.2684 21.4998Z"
                                        stroke="white" stroke-width="2.5" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path
                                        d="M18.1252 31.7083C18.1252 32.8021 17.8189 33.8376 17.2793 34.7126C17.0022 35.1793 16.6668 35.6021 16.2731 35.9375C16.2293 35.9959 16.1856 36.0396 16.1273 36.0833C15.1064 37.0021 13.7647 37.5417 12.2918 37.5417C10.5127 37.5417 8.92304 36.7395 7.87304 35.4854C7.84387 35.4416 7.8002 35.4125 7.77103 35.3688C7.59603 35.1646 7.4356 34.9459 7.30435 34.7126C6.76477 33.8376 6.4585 32.8021 6.4585 31.7083C6.4585 29.8708 7.30433 28.2229 8.646 27.1583C8.89391 26.9542 9.15638 26.7792 9.43346 26.6334C10.2793 26.1521 11.2564 25.875 12.2918 25.875C13.7502 25.875 15.0626 26.4 16.0835 27.2895C16.2585 27.4208 16.4189 27.5813 16.5647 27.7417C17.5272 28.7917 18.1252 30.1771 18.1252 31.7083Z"
                                        stroke="white" stroke-width="2.5" stroke-miterlimit="10" stroke-linecap="round"
                                        stroke-linejoin="round" />
                                    <path d="M14.4645 31.6792H10.1187" stroke="white" stroke-width="2.5"
                                        stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round" />
                                    <path d="M12.292 29.5503V33.9107" stroke="white" stroke-width="2.5"
                                        stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round" />
                                </svg>

                                <div class="title-card ml-3">
                                    Jumlah Pembelian OTS
                                </div>

                            </div>
                            <div class="total">
                                {{ formatMoney($pembelianOts) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



@push('javascript')
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.4/locale/id.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
    const setMessage = (id, pesan = '') => {
        var element = $(`#${id}`)
        element.html('')
        $('.alert-area').html('')
        if (pesan) {
            element.html(`<div class="alert alert-danger">${pesan}</div>`)
            setTimeout(() => {
                element.html('')
            }, 5000);
        }
    }

    $(document).ready(function () {
        $('#number').mask('00000000000000000000000');
        const btnFile = $(".btn-file")
        $("body").on("click", '#btn-add-link', function () {
            $("#modalAddLogo").modal("show")
        })

        $("body").on("click", '.edit-address', function () {
            $("#modalAddAddress").modal("show")
        })

        $('#btn-add-logo').click(function(){
            $("#logoFile").click();
        })


        $('#id_provinsi').trigger('change')
    })
</script>
@endpush('javascript')