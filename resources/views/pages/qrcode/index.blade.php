@extends('layout.master')
@push("css")
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.39.0/css/tempusdominus-bootstrap-4.min.css" crossorigin="anonymous" />
<style>
    .tribun {
        width: 100%;
        color: #fff;
        font-weight: 500;
        padding: 15px;
        border-radius: 5px;
        text-align: center;
    }

    th {
        vertical-align: middle !important;
    }

    .text-peserta {
        color: #898989;
        font-size: 12px;
        margin-right: 6px;
    }

    .total-peserta {
        font-size: 12px;
    }

    .select2-container--default .select2-results__option[aria-disabled=true] {
        display: none;
    }
    .bootstrap-datetimepicker-widget table td, .bootstrap-datetimepicker-widget table th {
        padding: unset!important;
        vertical-align: middle;
    }
    .bootstrap-datetimepicker-widget .collapse.show .timepicker {
        width: 13rem
    }
</style>
<link rel="stylesheet" href="{{ asset('assets/css/participant/participant.css') }}">
@endpush
@section('title', 'QR Code')

@section('content')
<div class="row mt-3">
    <div class="col-12">
        <ul class="nav nav-pills nav-fill">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="changeType" data-type="undangan"
                    href="javascript:void(0)">Undangan</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="changeType" data-type="umum"
                    href="javascript:void(0)">Umum</a>
            </li>
        </ul>
    </div>
</div>

<div class="row mt-3">
    <div class="card">
        <div class="card-body">
            <div class="d-flex justify-content-between align-items-center">
                <div class="total-item">
                    <span class="totalData"></span> Code
                </div>
                {{-- @endif --}}
                <div class="d-flex">
                    <div class="mr-2">
                        <select id="filterStatus" class="form-control">
                            <option value="all">Semua</option>
                            <option value="keluar">Sudah Keluar</option>
                            <option value="belum">Belum Redemp</option>
                            <option value="claim">Sudah Redemp</option>
                        </select>
                    </div>
                    <div class="mr-2">
                        <input type="text" placeholder="search" id="search" class="form-control">
                    </div>
                    <div class="mr-2">
                        <a href="{{ route('qrcode.export') }}" data-href="{{ route('qrcode.export') }}"
                            id="btnExport" target="_blank" class="btn btn-orange"><svg width="16" height="16"
                                viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M14 10V12.6667C14 13.0203 13.8595 13.3594 13.6095 13.6095C13.3594 13.8595 13.0203 14 12.6667 14H3.33333C2.97971 14 2.64057 13.8595 2.39052 13.6095C2.14048 13.3594 2 13.0203 2 12.6667V10"
                                    stroke="white" stroke-width="1.33333" stroke-linecap="round"
                                    stroke-linejoin="round" />
                                <path d="M11.3337 5.33333L8.00033 2L4.66699 5.33333" stroke="white"
                                    stroke-width="1.33333" stroke-linecap="round" stroke-linejoin="round" />
                                <path d="M8 2V10" stroke="white" stroke-width="1.33333" stroke-linecap="round"
                                    stroke-linejoin="round" />
                            </svg>
                            Export</a>
                            <a href="{{ route('checkout.tiket') }}" target="_blank" type="button"
                            class="btn btn-blue">Gelang Keluar</a>
                            <a href="{{ route('redemp.tiket') }}" target="_blank" type="button" class="btn btn-green">Redemp Tiket</a>
                    </div>
                </div>
            </div>
            <div class="mt-3">
                <table class="table table-striped" id="tableParticipant">
                    <thead>
                        <tr class="text-center">
                            <th class="text-center">Kode</th>
                            <th class="text-center">Waktu Keluar</th>
                            <th class="text-center">Loket</th>
                            <th class="text-center">Waktu Redemp</th>
                            <th class="text-center">Gate</th>
                            <th class="text-center">Status</th>
                            {{-- <th class="text-center" style="width: 140px">Aksi</th> --}}
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@push('modal')
<div class="modal fade" id="modalAddParticipant" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <form action="" method="POST" id="formSubmit" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" id="idPeserta">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tambah Peserta</h5>
                    <button class="btn btn-success close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-12 col-12">
                            <div class="form-group">
                                <label>Nama</label>
                                <input type="text" class="form-control" id="nama" name="nama" required
                                    placeholder="Masukkan nama peserta">
                            </div>
                        </div>
                        {{-- <div class="col-sm-6 col-12">
                            <div class="form-group">
                                <label>No. Handphone</label>
                                <input type="text" class="form-control number" id="no_hp" required name="no_hp"
                                    placeholder="Masukkan nomor handphone">
                            </div>
                        </div>
                        <div class="col-sm-6 col-12">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" id="email" name="email" required
                                    placeholder="Masukkan email">
                            </div>
                        </div> --}}
                        <div class="col-sm-6 col-12">
                            <div class="form-group">
                                <label>Gender <span class="text-danger">*</span></label>
                                <div style="padding: 10px 0;">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="genderMale" name="gender" value="Male"
                                            class="custom-control-input">
                                        <label class="custom-control-label" for="genderMale">Male</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="genderFemale" name="gender" value="Female"
                                            class="custom-control-input">
                                        <label class="custom-control-label" for="genderFemale">Female</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-12">
                            <div class="form-group">
                                <label>Kota Asal <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="kota_asal" name="kota_asal" required
                                    placeholder="Masukkan kota asal peserta">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-12">
                            <div class="form-group">
                                <label>Nomor Kamar</label>
                                <input type="text" name="room_number" id="room_number"
                                    placeholder="Masukkan nomor kamar" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6 col-12">
                            <div class="form-group">
                                <label>Waktu Shuttle</label>
                                <div class="input-group date" id="waktu_shuttle" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" id="waktu_shuttle_name" data-toggle="datetimepicker" data-target="#waktu_shuttle"/>
                                    <div class="input-group-append" data-target="#waktu_shuttle" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6 col-12">
                            <div class="form-group">
                                <label>Rute Kepulangan</label>
                                <input type="text" name="rute" id="rute"
                                    placeholder="Masukkan rute" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6 col-12">
                            <div class="form-group">
                                <label>Transportasi</label>
                                <input type="text" name="transportasi" id="transportasi"
                                    placeholder="Masukkan tranportasi" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-6 col-12">
                            <div class="form-group">
                                <label>Tanggal Kepulangan</label>
                                <div class="input-group date" id="date_return" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" id="date_return_name" data-toggle="datetimepicker" data-target="#date_return"/>
                                    <div class="input-group-append" data-target="#date_return" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-12">
                            <div class="form-group">
                                <label>Waktu Kepulangan</label>
                                <div class="input-group date" id="time_return" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" id="time_return_name" data-toggle="datetimepicker" data-target="#time_return"/>
                                    <div class="input-group-append" data-target="#time_return" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="alertMessage"></div>
                </div>

                <div class="modal-footer">
                    {{-- <button type="button" data-toggle="detail" class="btn btn-save" style="background-color: #ffa736!important">Reimburse</button> --}}
                    <button type="submit" id="store" class="btn btn-save">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>

@endpush
@push('javascript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.4/locale/id.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.39.0/js/tempusdominus-bootstrap-4.min.js" crossorigin="anonymous"></script>
<script>
    var table;
    var typingTimer;
    var doneTypingInterval = 500;
    var type = 'undangan'
    
    const setTable = (status = 'all') => {
        var isPic = $('#tableParticipant').attr('data-isPic')
        if (table) {
            table.destroy();
            table = '';
        }
        var columnFix = [
                {
                    data: 'kode',
                    name: 'kode',
                    className: 'text-left'
                },
                {
                    data: 'waktu_keluar',
                    name: 'waktu_keluar',
                    className: 'text-left'
                },
                {
                    data: 'loket_keluar',
                    name: 'loket_keluar',
                    className: 'text-left'
                },
                {
                    data: 'waktu_redemp',
                    name: 'waktu_redemp',
                    className: 'text-left'
                },
                {
                    data: 'loket',
                    name: 'loket',
                    className: 'text-left'
                },
                {
                    data: 'status',
                    name: 'status',
                },
            ];
        table = $("#tableParticipant").DataTable({
            processing: true,
            serverSide: false,
            ajax: location.href+'?filterStatus='+status+'&type='+type,
            autoWidth: false,
            lengthChange: false,
            order: [],
            ordering: true,
            searching: true,
            pageLength: 50,
            columns: columnFix,
            initComplete: function(settings, json) {
                $('.totalData').text(formatMoney(json.recordsTotal))
            }
        });

        table.on('draw', function () {
            $('.dataTables_filter').remove();
        })
    }

    const setMessage = (pesan = '') => {
        var element = $('#alertMessage')
        element.html('')
        if (pesan) {
            element.html(`<div class="alert alert-danger mt-3">${pesan}</div>`)
            setTimeout(() => {
                element.html('')
            }, 5000);
        }
    }

    const formatMoney = (a) => {
        if (!a) {
            return '0';
        }

        return a.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
    }

    $(document).ready(function () {
        setTable();
        $('.number').mask('000000000000000000');
        $('.time_mask').mask("00:00")

        $('#filterStatus').change(function() {
            var val = $(this).val()
            var old = $('#btnExport').attr('data-href');
            $('#btnExport').attr(`href`, `${old}?status=${val}&type=${type}`);
            setTable(val);
        })

        $('[data-toggle="changeType"]').click(function(){
            var ty = $(this).attr('data-type');
            if (ty) {
                $('[data-toggle="changeType"]').removeClass('active')
                $(this).addClass('active')
                type = ty;
                var status = $('#filterStatus').find('option:selected').val();
                setTable(status);
            }
        })
    })


    $('#search').on("keyup", function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(function() {
            var val = $('#search').val();
            table.search(val).draw();        
        }, doneTypingInterval);
    })

</script>
@endpush('javascript')