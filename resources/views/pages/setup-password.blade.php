@extends('layout.master')

@section('title', 'Set Password')

@push('css')

<link rel="stylesheet" href="{{ asset('assets/css/login/login.css') }}">
@endpush

@section('content')

<section class="login">
    <div class="row ">

        <div class="col-md-6 d-table col-content">

            <div class=" d-table-cell align-middle content">
                <div class="card">
                    <div class="img-logo">
                    <img src="{{ asset('assets/img/logo/logo.svg') }}" alt="logo" width="100" class="logo">
                    </div>
                    <div class="title-form">
                        <div class="welcome-text">
                        Set Password
                        </div>
                        <div class="desc-text">
                        Selamat datang! Harap isi dengan lengkap.
                        </div>
                    </div>
                    <form class="needs-validation mt-5" method="POST" id="LoginForm" action="{{route('save-password')}}">
                        @csrf
                        <input type="hidden" name="uniqId" value="{{ $id }}">
                        <div class="form-group">
                            <label>Password</label>
                            <div class="password-area position-relative">
                                <input type="password" name="password" class="form-control" id="password" placeholder="Password" required>
                                <div class="" data-toggle="change_password" style="position: absolute;right: 10px;top: 50%;transform: translateY(-50%); cursor: pointer">
                                    <span class="fas fa-eye"></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Ulangi Password</label>
                            <div class="password-area position-relative">
                                <input type="password" name="passwordConfirm" class="form-control" id="passwordConfirm" placeholder="Ulangi Password" required>
                                <div class="" data-toggle="change_password" style="position: absolute;right: 10px;top: 50%;transform: translateY(-50%); cursor: pointer">
                                    <span class="fas fa-eye"></span>
                                </div>
                            </div>
                            <div class="form-error mt-1 text-danger" id="error_password"></div>
                        </div>
                        <button class="btn btn-form mt-3 mb-4 d-block" id="submitForm" type="submit">
                            <span class="btn-login">Set Password</span></button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6 text-center d-table col-banner">
            <div class=" banner d-table-cell align-middle">
            <img src="{{ asset('assets/img/img-banner.svg') }}" class="img-banner">
                  
        </div>
    </div>
</section>

@endsection

@push('javascript')
    <script>
        $('body').on('click', '[data-toggle="change_password"]', function(e) {
            const input = $(this).closest('.form-group').find('input')
            const icon = $(this).closest('.password-area').find('span');

            if (input.attr('type') == 'text') {
                input.attr('type', 'password')
                icon.removeClass('fa-eye').addClass('fa-eye-slash')
            } else {
                input.attr('type', 'text');
                icon.removeClass('fa-eye-slash').addClass('fa-eye')
            }
        })

        $('[type="password"]').on('keyup', function(){
            $('#error_password').text('');
            $('#submitForm').attr("disabled", false);
            var val = $('#password').val()
            var val2 = $('#passwordConfirm').val()
            
            if (val && val.length < 8) {
                $('#submitForm').attr("disabled", true);
                $('#error_password').text('Password minimal 8 karakter')
            } else if (val != val2) {
                $('#submitForm').attr("disabled", true);
                $('#error_password').text('Password tidak sama')
            }
        });
    </script>
@endpush