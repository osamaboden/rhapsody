@extends('layout.master')

@section('title', 'Login')

@push('css')

<link rel="stylesheet" href="{{ asset('assets/css/login/login.css') }}">
@endpush

@section('content')

<section class="login">
    <div class="row ">

        <div class="col-md-6 d-table col-content">

            <div class=" d-table-cell align-middle content">
                <div class="card">
                    <div class="img-logo">
                    <img src="{{ asset('assets/img/logo/logo_rhapsody.png') }}" title="logo_rhapsody" alt="logo" class="logo">
                    </div>
                    <div class="title-form">
                        <div class="welcome-text">
                        Login
                        </div>
                        <div class="desc-text">
                        Selamat datang! Harap isi dengan lengkap.
                        </div>
                    </div>
                    <form class="needs-validation mt-5" method="POST" id="LoginForm" action="{{route('aksilogin')}}">
                        @if ($message = Session::get('success'))
                        <div class="alert alert-info alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ $message }}</strong>
                        </div>
                        @endif

                        @if ($message = Session::get('error'))
                        <div class="alert alert-danger alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>{{ $message }}</strong>
                        </div>
                        @endif
                        @csrf
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" class="form-control" placeholder="Username"
                                aria-label="Username" aria-describedby="basic-addon1" required>
                        </div>
                        <div class="form-group pt-2">
                            <label>Password</label>
                            <div class="password-area position-relative">
                                <input type="password" name="password" class="form-control" id="password" placeholder="Password" required>
                                <div class="" data-toggle="change_password" style="position: absolute;right: 10px;top: 50%;transform: translateY(-50%); cursor: pointer">
                                    <span class="fas fa-eye"></span>
                                </div>
                            </div>
                            <div class="form-error" id="error_password"></div>
                        </div>
                        <button class="btn btn-form mt-3 mb-4 d-block" id="submitForm" type="submit"><i
                                class="fas fa-spinner fa-spin" style="display:none;"></i>
                            <span class="btn-login">Login</span></button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6 text-center d-table col-banner">
            <div class=" banner d-table-cell align-middle">
            <img src="{{ asset('assets/img/login.png') }}" class="img-banner">
                  
        </div>
    </div>
</section>

@endsection

@push('javascript')
    <script>
        $('[data-dismiss="alert"]').on('click', function(){
            $(this).parent().remove();
        })
        $('body').on('click', '[data-toggle="change_password"]', function(e) {
            const input = $(this).closest('.form-group').find('input')
            const icon = $(this).closest('.password-area').find('span');

            if (input.attr('type') == 'text') {
                input.attr('type', 'password')
                icon.removeClass('fa-eye').addClass('fa-eye-slash')
            } else {
                input.attr('type', 'text');
                icon.removeClass('fa-eye-slash').addClass('fa-eye')
            }
        })
    </script>
@endpush