<!-- Modal -->

<div class="modal fade" id="modalSuccess" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body text-center">
                <svg width="100" height="100" viewBox="0 0 178 178" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="89" cy="89" r="89" fill="#F0FFF0" />
                    <circle cx="89.0004" cy="89" r="68.1235" fill="#C8FFC7" />
                    <circle cx="89.0002" cy="89" r="50.5432" fill="url(#paint0_linear_102_570)" />
                    <path d="M112 73.6875L80.375 105.312L66 90.9375" stroke="white" stroke-width="14"
                        stroke-linecap="round" stroke-linejoin="round" />
                    <defs>
                        <linearGradient id="paint0_linear_102_570" x1="89.0002" y1="38.4568" x2="89.0002" y2="139.543"
                            gradientUnits="userSpaceOnUse">
                            <stop stop-color="#69F45D" />
                            <stop offset="1" stop-color="#1A9E28" />
                        </linearGradient>
                    </defs>
                </svg>

                <div class="modal-title mt-3">
                Check In <span style="color:#45D42E;">Success! </span>
                </div>
                <div class="modal-desc mt-2">
                Please enter and enjoy the event
                </div>
            </div>
            <div class="modal-footer tex-center justify-content-center">
                <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>