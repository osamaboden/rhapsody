<ul class="nav nav-pils">
    <li>
        <a href="#" class="nav-link {{ findActiveSidebar('active', ['setting.index']) }}">Data Events</a>
    </li>
    <li>
        <a href="#" class="nav-link {{ findActiveSidebar('active', ['setting.template-event']) }}">Events Website</a>
    </li>
    <li>
        <a href="#" class="nav-link {{ findActiveSidebar('active', ['setting.line-up']) }}">Line Up</a>
    </li>
</ul>