<!-- Modal -->

<div class="modal fade" id="modalFailed" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body text-center">
                <svg width="100" height="100" viewBox="0 0 162 162" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="81" cy="81" r="81" fill="#FFF8F8" />
                    <circle cx="81" cy="81" r="62" fill="#FAD2DA" />
                    <circle cx="81" cy="81" r="46" fill="url(#paint0_linear_74_677)" />
                    <path d="M94 68L68 94" stroke="white" stroke-width="12" stroke-linecap="round"
                        stroke-linejoin="round" />
                    <path d="M68 68L94 94" stroke="white" stroke-width="12" stroke-linecap="round"
                        stroke-linejoin="round" />
                    <defs>
                        <linearGradient id="paint0_linear_74_677" x1="81" y1="35" x2="81" y2="127"
                            gradientUnits="userSpaceOnUse">
                            <stop stop-color="#FF7D89" />
                            <stop offset="1" stop-color="#DD1D37" />
                        </linearGradient>
                    </defs>
                </svg>
                <div class="modal-title mt-3">
                    Sorry, Your Registration <span style="color:#ff2c00;">Failed</span> 
                </div>
                <div class="modal-desc mt-2">
                    Email and phone number have been used before, please use another email and phone number.
                </div>
            </div>
            <div class="modal-footer tex-center justify-content-center">
                <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>