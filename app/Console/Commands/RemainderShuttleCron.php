<?php

namespace App\Console\Commands;

use App\Jobs\EmailRemainderShuttle;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class RemainderShuttleCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'shuttle-remainder:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Artisan::call("queue:retry all");
        $now = Carbon::now()->format("Y-m-d H:i:s");
        $data = DB::table("peserta")->whereRaw("email_shuttle = 0 AND TIMESTAMPDIFF(MINUTE, '$now', waktu_shuttle) < 180")->select("nama", "email", "waktu_shuttle", "email_shuttle")->limit(20)->get();
        DB::table("peserta")->whereRaw("email_shuttle = 0 AND TIMESTAMPDIFF(MINUTE, '$now', waktu_shuttle) < 180")->update([
            'email_shuttle' => '1'
        ]);
        foreach ($data as $key => $value) {
            if ($value->email_shuttle == "0") {
                $details = [
                    'nama' => $value->nama,
                    'email' => $value->email,
                    'waktu_shuttle' => convertDate($value->waktu_shuttle, "d F Y H:m", 'id'),
                    'link_asset' => asset('') . '/',
                    'from' => [
                        'address' => env('MAIL_FROM_ADDRESS'),
                        'name' => 'Remainder - Corteva'
                    ]
                ];

                dispatch(new EmailRemainderShuttle($details));
            }
        }
    }
}
