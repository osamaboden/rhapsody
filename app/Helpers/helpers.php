<?php 
use Carbon\Carbon;
use Carbon\CarbonInterface;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

function findActiveSidebar($class, $listname) {
    $current_route=Route::currentRouteName();

    if (is_array($listname)) {
        if (in_array($current_route, $listname)) {
            return $class;
        }
    } else {
        if ($current_route==$listname) {
            return $class;
        }
    }
}


function createObjectLineUp($id, $nama, $nama_file){
    $obj = new stdClass();
    $obj->id = $id;
    $obj->nama = $nama;
    $obj->nama_file = $nama_file;

    return $obj;
}

function createObjectSponsor($id, $nama_file){
    $obj = new stdClass();
    $obj->id = $id;
    $obj->nama_file = $nama_file;

    return $obj;
}

function convertDate($time, $format = "Y-m-d", $locale = 'en') {
    if ($locale == 'id') {
        $date = Carbon::parse($time)->locale('id');
        return $date->translatedFormat($format);
    }
    $date = Carbon::parse($time);

    return $date->format($format);
}

function getEmbedUrl($link) {
    preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $link, $matches);
    if (isset($matches[1])) {
        return 'https://www.youtube.com/embed/'.$matches[1];
    } else {
        return null;
    }
}

function generateRandomString($length = 10, $type = 'string') {
    if ($type == 'angka') {
        $characters = '1234567890';
    } else if ($type == 'alfanumeric') {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abdcefghijklmnopqrstuvwxyz';
    } else {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    }
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[random_int(0, $charactersLength - 1)];
    }
    return $randomString;
}

function formatMoney($number){
    return number_format($number, 0, '', '.');
}

function checkRoute() {
    if (auth('web')->check()) {
        //success
        return true;
    } else {
        return false;
    }
}

function convertTime($jamMulai, $jamSelesai) {
    $mulai = substr($jamMulai, 0,5);
    $selesai = substr($jamSelesai, 0,5);
    return $mulai.' - '.$selesai;
}

function videoConvert($video){
    $convert = str_replace('watch?v=', 'embed/', $video);
    return  $convert;
}

function checkNumber($number) {
    $number = str_replace(['-', ''], ['', ''],$number);

    if (substr($number, 0, 2) == '08') {
        return '62'.substr($number, 1);
    } else if ((substr($number, 0, 1) == '8')) {
        return '62'.$number;
    } else if ($number) {
        return '62'.$number;
    } else {
        return '';
    }
}

function checkDir($dir = '') {
    $path = storage_path("app/public/$dir");
    if (!Storage::directoryExists($path)) {
        Storage::makeDirectory($path);
        exec("chown -R apache:apache $path");
        exec("chmod -R 755 $path");
        exec("chcon -R -t httpd_sys_rw_content_t $path");
    }
}