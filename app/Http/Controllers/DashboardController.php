<?php

namespace App\Http\Controllers;

use App\Models\Event;
use App\Models\Kabupaten;
use App\Models\Kecamatan;
use App\Models\PembelianOtsModel;
use App\Models\Provinsi;
use App\Models\Peserta;
use App\Models\TiketModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class DashboardController extends Controller
{
    public function index()
    {
        $tiketOut = TiketModel::whereNotNull('waktu_keluar')->where('waktu_keluar', '!=', '')->whereBetween('increment', [1, 5000])->count();
        $tiketRedemp = TiketModel::whereNotNull('waktu_redemp')->where('waktu_redemp', '!=', '')->whereBetween('increment', [1, 5000])->count();
        $loket = auth()->user()->type != 'admin' ? auth()->user()->loket : '';
        $pembelianOts = PembelianOtsModel::when($loket, function($query, $params) {
            if ($params) {
                $query->where('loket', $params);
            }
        })->where('metode_bayar', "!=", 'tiket.com')->sum('jumlah_tiket');

        return view('pages.dashboard.index', compact('tiketOut', 'pembelianOts', 'tiketRedemp'));
    }

    public function getKode() {
        $kode = generateRandomString(6);
        $cek = TiketModel::where('kode', $kode)->count();
        if ($cek > 0) {
            return $this->getKode();
        }

        return $kode;
    }

    public function generateQrCode($file, $code)
    {
        $image = QrCode::format('png')
            ->size(500)->errorCorrection('H')->style('square')->eye('square')
            ->backgroundColor(255,255,255)
            ->margin(2)
            ->generate($code);
        $output_file = '/public/qr_code/' . $file . '.png';
        Storage::disk('local')->put($output_file, $image);
    }
}
