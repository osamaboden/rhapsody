<?php

namespace App\Http\Controllers;

use App\Models\GroupPesertaPic;
use App\Models\ParamsDecode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    public function index()
    {
        // dd(Hash::make('01Xrhapsody2024@#rhapsody'));
        if (Auth::guard('web')->check()) {
            return redirect()->route('dashboard.index');
        } else{
            return view('pages.login');
        }
    }

    public function aksi_login(Request $request)
    {
        try {
            $data = [
                'email' => $request->input('email'),
                'password' => '01X'.$request->input('password').'@#rhapsody',
            ];
            
            if (auth('web')->attempt($data)) {
                return redirect()->route('dashboard.index');
            }else{
                Session::flash('error', 'Email atau password anda salah');
                return redirect()->route('login'); 
            }
        } catch (\Throwable $th) {
            Log::info($th);
        }
    }

    public function aksi_logout()
    {
        // if (Auth::guard('pic')->check()){
        //     dd(1);
        //     Auth::guard('pic')->logout();
        // } else {
        //     Auth::guard('web')->logout();
        // }
        Auth::logout();
        session()->flush();

        return redirect()->route('login');
    }

}