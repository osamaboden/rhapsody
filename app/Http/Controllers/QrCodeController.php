<?php

namespace App\Http\Controllers;

use App\Exports\ExportQrCode;
use App\Models\TiketModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class QrCodeController extends Controller
{
    //

    public function index(Request $request) {
        if ($request->ajax()) {
            $status = $request->filterStatus != 'all' && $request->filterStatus != '' ? $request->filterStatus : '';
            $type = $request->type != "" && $request->type ?  $request->type : "undangan";
            $data = TiketModel::when($status, function ($query, $params) {
                    if ($params == 'not-claim') {
                        $query->where('status', 'belum');
                    } else if ($params == 'claim') {
                        $query->where('status', 'claim');
                    } else if ($params == 'keluar') {
                        $query->where('status', 'keluar');
                    }
                })
                ->when($type, function($query, $params) {
                    if ($params == 'umum') {
                        $query->limit('4000')->offset('1000');
                    } else {
                        $query->limit('1000')->offset('0');
                    }
                })
                ->select('*')
                ->orderByRaw("increment ASC")->get();
            return DataTables::of($data)
                // ->addIndexColumn()
                ->setRowAttr([
                    'data-id' => function ($row) {
                        return $row->id;
                    },
                    'data-kode' => function ($row) {
                        return $row->kode;
                    },
                    'data-status' => function ($row) {
                        return $row->status;
                    },
                    'data-loket' => function ($row) {
                        return $row->loket ?? '-';
                    },
                    'data-waktu_redemp' => function ($row) {
                        return $row->waktu_redemp ?? '-';
                    },
                    'data-qr' => function ($row) {
                        return asset('storage/qr_code/' . $row->kode . '.png');
                    },
                    'data-toggle' => 'showDetail',
                    'class' => function ($row) {
                        return 'pointer text-center';
                    },
                ])
                ->editColumn('kode', function ($row) {
                    return $row->kode != '' ? $row->kode : '-';
                })
                ->editColumn('loket', function ($row) {
                    return $row->loket != '' ? $row->loket : '-';
                })
                ->editColumn('loket_keluar', function ($row) {
                    return $row->loket_keluar != '' ? $row->loket_keluar : '-';
                })
                ->editColumn('status', function ($row) {
                    // 'available','used','cancel'
                    if ($row->status == "belum") {
                        return 'Belum';
                    } else if ($row->status == "claim") {
                        return 'Claim';
                    } else if ($row->status == "keluar") {
                        return "Keluar";
                    }
                })
                ->editColumn('waktu_redemp', function($row) {
                    $html = "";
                    if ($row->waktu_redemp) {
                        $html .= convertDate($row->waktu_redemp, 'd M Y H:i', 'id');
                    } else {
                        $html = '-';
                    }

                    return $html;
                })
                ->editColumn('waktu_keluar', function($row) {
                    $html = "";
                    if ($row->waktu_keluar) {
                        $html .= convertDate($row->waktu_keluar, 'd M Y H:i', 'id');
                    } else {
                        $html = '-';
                    }

                    return $html;
                })
                ->rawColumns(['status'])
                ->make(true);
        }

        return view('pages.qrcode.index');
    }

    public function export(Request $request) {
        $status = $request->status != 'all' && $request->status != '' ? $request->status : '';
        $type = $request->type != "" && $request->type ?  $request->type : "undangan";
        $file_name = 'QR_CODE_' .($type == 'umum' ? 'UMUM_' : 'UNDANGAN_'). date('Y_m_d_H_i_s') . '.xlsx';
        return Excel::download(new ExportQrCode($status, $type), $file_name);
    }
}
