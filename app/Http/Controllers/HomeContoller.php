<?php

namespace App\Http\Controllers;


use App\Models\TiketModel;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;  
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class HomeContoller extends Controller
{

    public function checkin()
    {
        $type = "redemp";
        return view("pages.registration.registration", compact('type'));
    }

    public function checkout()
    {
        $type = "checkout";
        return view("pages.registration.registration", compact('type'));
    }

    public function pesertaAll()
    {
        $list = DB::table('tiket')
            ->select('id', 'kode', 'status','waktu_redemp', 'loket')
            ->orderByRaw("increment ASC")
            ->get();

        return response()->json([
            'is_success' => true,
            'list' => $list
        ], 200);
    }

    public function re_registrationCheck(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'uniq_code' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'is_success' => false,
                    'error' => $validator->errors(),
                ], 200);
            }

            $uniqCode   = $request->input('uniq_code');
            $checkData  = TiketModel::where('kode', $uniqCode)->first();

            if (empty($checkData)) {
                return response()->json([
                    'is_success' => false,
                    'error' => 'tiket-not-found',
                ], 200);
            }

            if ($request->input('type') == "redemp") {
                if ($checkData->status == "claim" || (!is_null($checkData->waktu_redemp) && $checkData->waktu_redemp != "")) {
                    throw new Exception("tiket-has-claim");
                }
    
                TiketModel::where('id', $checkData->id)->update([
                    'status' => 'claim',
                    'loket' => (auth()->check()) ? auth()->user()->loket : $request->input('loket'),
                    'waktu_redemp' => Carbon::now(),
                ]);
            } else {
                if ($checkData->status == "keluar" || (!is_null($checkData->waktu_keluar) && $checkData->waktu_keluar != "")) {
                    throw new Exception("tiket-has-out");
                }
    
                TiketModel::where('id', $checkData->id)->update([
                    'status' => 'keluar',
                    'loket_keluar' => (auth()->check()) ? auth()->user()->loket : $request->input('loket'),
                    'waktu_keluar' => Carbon::now(),
                ]);
            }

            return response()->json([
                'is_success' => true,
            ], 200);
        } catch (\Throwable $th) {
            Log::info($th);
            return response()->json([
                'is_success' => false,
                'error' => $th->getMessage(),
            ], 200);
        }
    }
}
// http://localhost:8000/invite/ececc1bff67f43cbada327d2631f04bb