<?php

namespace App\Http\Controllers;

use App\Exports\ExportPembelianOTS;
use App\Models\PembelianOtsModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class PembelianOtsController extends Controller
{
    //
    public function index(Request $request) {
        if ($request->ajax()) {
            DB::statement(DB::raw('set @rownum=0'));
            $loket = auth()->user()->type != 'admin' ? auth()->user()->loket : '';
            $data = PembelianOtsModel::
                    when($loket, function($query, $params) {
                        if ($params) {
                            $query->where('loket', $params);
                        }
                    })
                    ->select("*", DB::raw("@rownum  := @rownum  + 1 AS rownum"))->orderBy('created_at', 'desc')->get();
            return DataTables::of($data)
                    ->addIndexColumn()
                    ->setRowAttr([
                        'data-id' => function ($row) {
                            return $row->id;
                        },
                        "data-toggle" => 'showModal',
                        'class' => function($row) {
                            return 'pointer';
                        },
                    ])
                    ->addColumn('created_at', function($row) {
                        $html = "";
                        if ($row->created_at) {
                            $html .= convertDate($row->created_at, 'd M Y H:i', 'id');
                        } else {
                            $html = '-';
                        }
    
                        return $html;
                    })
                    ->editColumn('nama', function ($row) {
                        return $row->nama != '' ? $row->nama : '-';
                    })
                    ->editColumn('no_hp', function ($row) {
                        return $row->no_hp != '' ? $row->no_hp : '-';
                    })
                    ->editColumn('kode_booking', function ($row) {
                        return $row->kode_booking != '' ? $row->kode_booking : '-';
                    })
                    ->editColumn('email', function ($row) {
                        return $row->email != '' ? $row->email : '-';
                    })
                    ->make(true);
        }
        $totalActive = PembelianOtsModel::count();
        $metodeBayar = array_unique(array_merge(['tiket.com', 'QRIS'], PembelianOtsModel::distinct('metode_bayar')->pluck('metode_bayar')->toArray()));
        return view('pages.pembelian_ots.index', ['total' => $totalActive, 'metodeBayar' => $metodeBayar    ]);
    }

    public function store(Request $request) {
        $hasil = [
            'is_success' => false,
            'response_code'  => 400,
            'msg'   => ''
        ];

        DB::beginTransaction();
        try {
            $id = $request->input('id'); 
            $data = $request->only(['nama', 'email', 'no_hp', 'metode_bayar', 'jumlah_tiket', 'kode_booking']);

            if ($id != '') {
                // PembelianOtsModel::where('id', $id)->update($data);
                // $hasil['response_code'] = 200;
                // $data['id'] = $id;
            } else {
                $data['id'] = Str::uuid();
                $data['jumlah_tiket'] = $data['jumlah_tiket'] ?? '1';
                $data['loket'] = (auth()->check()) ? auth()->user()->loket : $request->input('loket');
                PembelianOtsModel::create($data);
                $hasil['response_code'] = 201;
            }
            DB::commit();
            $hasil['is_success'] = true;
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::info($th);
            $hasil['msg'] = $th->getMessage();
        }

        return response()->json($hasil, $hasil['response_code']);
    }

    public function export(){
        $loket = auth()->user()->type != 'admin' ? auth()->user()->loket : '';
        $file_name = 'OTS_' .($loket != "" ? 'LOKET-'.$loket.'_' : ''). date('Y_m_d_H_i_s') . '.xlsx';
        return Excel::download(new ExportPembelianOTS($loket), $file_name);
    }
}
