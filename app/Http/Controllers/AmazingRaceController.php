<?php

namespace App\Http\Controllers;

use App\Models\AmazingRaceLokasiFoto;
use App\Models\AmazingRaceLokasi;
use App\Models\BoothModel;
use App\Models\Peserta;
use App\Models\Seating;
use App\Models\SeatingMeja;
use App\Models\SeatingMejaPeserta;
use App\Models\Team;
use App\Models\TeamBooth;
use App\Models\TeamLokasi;
use App\Models\TeamPeserta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Yajra\DataTables\Facades\DataTables;

class AmazingRaceController extends Controller
{
    //

    public function index(Request $request){
        $dataSeating = Seating::where('event_id', auth()->user()->id_event)->where("status", "aktif")->select("id", "jumlah_permeja")->get();
            
        if ($request->ajax()) {
            DB::statement(DB::raw('set @rownum=0'));
            $data = Team::with('teamLokasi.lokasi')->where('event_id', auth()->user()->id_event)
                            ->select("id", "nama", 'nomor_mobil', DB::raw("@rownum  := @rownum  + 1 AS rownum"))
                            ->orderByRaw("CAST(SUBSTRING(nama, 6) AS UNSIGNED) ASC")->get();
            return DataTables::of($data)
                    ->addIndexColumn()
                    ->setRowAttr([
                        'data-id' => function ($row) {
                            return $row->id;
                        },
                        'data-route' => function($row){
                            return route('amazing-race.group-detail', ['id' => $row->id]);
                        },
                        "data-toggle" => 'showModal',
                        'class' => function($row) {
                            return 'pointer';
                        },
                    ])
                    ->addColumn('lokasi', function($row){
                        $lokasi = [];
                        $teamLokasi = TeamLokasi::with("lokasi")->where('team_id', $row->id)->get();
                        foreach ($teamLokasi as $key => $value) {
                            $nama = ($value->lokasi) ? $value->lokasi->nama : '';
                            if (!$nama) {
                                $aa = AmazingRaceLokasi::where('id', $value->lokasi_id)->first();
                                $nama = $aa->nama;
                            }
                            array_push($lokasi, $nama);
                        }

                        return count($lokasi) > 0 ? implode(', ',$lokasi) : 'Belum ditentukan';
                    })
                    ->editColumn('nomor_mobil', function($row){
                        return $row->nomor_mobil ?? "Belum ditentukan";
                    })
                    ->make(true);
        }
        $totalActive = Team::where('event_id', auth()->user()->id_event)->count();
        return view('pages.amazing-race.index', ['total' => $totalActive]);
    }

    public function createGroup(Request $request) {
        $hasil = [
            'is_success' => false,
            'response_code'  => 400,
            'msg'   => ''
        ];

        try {
            $totalGroup = $request->input('jumlah_group');
            $totalPeserta = Peserta::where('id_event', auth()->user()->id_event)->count();
            $pesertaPerGroup = $totalPeserta >= $totalGroup ? floor($totalPeserta/$totalGroup) : 1;
            $peserta = Peserta::where('id_event', auth()->user()->id_event)->select('id', 'nama')->inRandomOrder()->get()->toArray();

            shuffle($peserta);
            shuffle($peserta);

            $arrTeam = [];
            $arrPeserta = [];
            $totalTeam = 0;
            foreach (array_chunk($peserta, $pesertaPerGroup) as $keyGroup => $group) {
                if ($totalTeam == $totalGroup) {
                    $i = 0;
                    foreach ($group as $key => $value) {
                        array_push($arrPeserta, [
                            'team_id' => $arrTeam[$i]['id'],
                            'peserta_id' => $value['id'],
                        ]);
                        $i++;

                        if (array_key_exists($i, $arrTeam)) {
                            $i = 0;
                        }
                    }
                } else {
                    $item = [
                        'id' => Str::uuid(),
                        'nama' => 'Team '.($keyGroup + 1),
                        "event_id" => auth()->user()->id_event
                    ];
    
                    array_push($arrTeam, $item);
                    $totalTeam++;
                    foreach ($group as $key => $value) {
                        array_push($arrPeserta, [
                            'team_id' => $item['id'],
                            'peserta_id' => $value['id'],
                        ]);
                    }
                }
            }

            $boothId = BoothModel::where("status", "aktif")->where('event_id', auth()->user()->id_event)->inRandomOrder()->pluck("id")->toArray();
            shuffle($boothId);
            $lokasiId = AmazingRaceLokasi::where("status", "aktif")->where('event_id', auth()->user()->id_event)->inRandomOrder()->pluck("id")->toArray();
            shuffle($lokasiId);
            $arrBooth = [];
            $arrLokasi = [];
            $indexLokasi = 0;
            foreach ($arrTeam as $key => $value) {
                $urutan = 1;
                foreach ($boothId as $i => $v) {
                    array_push($arrBooth, [
                        'team_id' => $value['id'],
                        'booth_id' => $v,
                        'urutan' => $urutan,
                    ]);
                    $i++;
                    $urutan++;
                }

                $firstElement = array_shift($boothId);
                array_push($boothId, $firstElement);

                if ($lokasiId[$indexLokasi]) {
                    array_push($arrLokasi, [
                        'team_id' => $value['id'],
                        'lokasi_id' => $lokasiId[$indexLokasi],
                        'urutan' => 1,
                    ]);

                    $indexLokasi++;
                }

                if ($indexLokasi == count($lokasiId)) {
                    $indexLokasi = 0;
                }
            }

            $teamOld = Team::where('event_id', auth()->user()->id_event)->pluck('id')->toArray();
            Team::where('event_id', auth()->user()->id_event)->delete();
            TeamPeserta::whereIn('team_id', $teamOld)->delete();
            TeamLokasi::whereIn('team_id', $teamOld)->delete();
            TeamBooth::whereIn('team_id', $teamOld)->delete();

            Team::insert($arrTeam);
            TeamPeserta::insert($arrPeserta);
            TeamLokasi::insert($arrLokasi);
            TeamBooth::insert($arrBooth);

            $dataSeating = Seating::where('event_id', auth()->user()->id_event)->where("status", "aktif")->select("id", "jumlah_permeja")->get();
            
            foreach ($dataSeating as $key => $value) {
                $this->shuffleSeatingByTeam($value->id, $value->jumlah_permeja, $totalPeserta);
            }

            $hasil['is_success'] = true;
            $hasil['response_code'] = 201;
            $hasil['total'] = count($arrTeam);
        } catch (\Throwable $th) {
            Log::info($th);
            $hasil['msg'] = $th->getMessage();
        }


        return response()->json($hasil, $hasil['response_code']);
    }

    public function shuffleSeatingByTeam($seatingId, $jumlahPermeja, $totalPeserta) {
        $jumlahMeja = ceil($totalPeserta/$jumlahPermeja);
        $arrMeja = array_fill(0, $jumlahMeja, ["id" => "", "seating_id" => $seatingId, "jumlah_kursi" => $jumlahPermeja, 'list' => []]);
        foreach ($arrMeja as $key => $value) {
            $arrMeja[$key]['id'] = Str::uuid()->toString();
        }
        $team = Team::where('event_id', auth()->user()->id_event)->orderByRaw("CAST(SUBSTRING(nama, 6) AS UNSIGNED) ASC")->pluck('id')->toArray();
        
        $arr = [];
        $jumlahPeserta = 0;
        foreach ($team as $it => $vt) {
            $peserta = TeamPeserta::where('team_id', $vt)->pluck('peserta_id')->toArray();
            $jumlahPeserta += count($peserta); 
            $perMeja = ceil($jumlahPermeja / 2);
            
            foreach ($arrMeja as $im => $vm) {
                $sisaKursi = $arrMeja[$im]['jumlah_kursi'] - count($arrMeja[$im]['list']);
                if ($sisaKursi >= $perMeja && count($peserta) >= $perMeja) {
                    for ($i=0; $i < $perMeja; $i++) { 
                        array_push($arr, [
                            "meja_id" => $vm['id'],
                            'peserta_id' => $peserta[$i],
                            "seating_id" => $seatingId,
                        ]);
                        array_push($arrMeja[$im]['list'], $peserta[$i]);
                    }
                    array_splice($peserta, 0, $perMeja);
                } else if ($sisaKursi && count($peserta) >= $sisaKursi) {
                    for ($i=0; $i < $sisaKursi; $i++) { 
                        array_push($arr, [
                            "meja_id" => $vm['id'],
                            'peserta_id' => $peserta[$i],
                            "seating_id" => $seatingId,
                        ]);
                        array_push($arrMeja[$im]['list'], $peserta[$i]);
                    }
                    array_splice($peserta, 0, $sisaKursi);
                } else if ($sisaKursi > count($peserta) && count($peserta) <= $perMeja) {
                    for ($i=0; $i < count($peserta); $i++) { 
                        array_push($arr, [
                            "meja_id" => $vm['id'],
                            'peserta_id' => $peserta[$i],
                            "seating_id" => $seatingId,
                        ]);
                        array_push($arrMeja[$im]['list'], $peserta[$i]);
                    }
                    array_splice($peserta, 0, count($peserta));
                }
                if (count($peserta) == 0) {
                    break;
                }
            }
            
            // dd($perMeja, $sisaPeserta, $jumlahMejaTeam, $vt);
        }
        
        $insertMeja = array_map(function($value, $k) {
            unset($value['list']);
            unset($value['jumlah_kursi']);
            $value['nama'] = $k+1;
            return $value;
        }, $arrMeja, array_keys($arrMeja));
        $arrayPesertaId = array_map(function($value, $k) {
            return $value['peserta_id'];
        }, $arr, array_keys($arr));
        $pesertaBelum = DB::table("peserta")->whereNotIn("id", $arrayPesertaId)->get();
        SeatingMeja::where("seating_id", $seatingId)->delete();
        SeatingMejaPeserta::where("seating_id", $seatingId)->delete();
        $end = end($insertMeja);
        foreach ($pesertaBelum as $key => $value) {
            array_push($arr, [
                "meja_id" => $end['id'],
                'peserta_id' => $value->id,
                "seating_id" => $seatingId,
            ]);
        }

        SeatingMeja::insert($insertMeja);
        SeatingMejaPeserta::insert($arr);

    }

    public function detail($id){
        $data = Team::with('teamLokasi.lokasi', "teamBooth.booth", 'teamPeserta.peserta')->where('id', $id)->first();

        if(is_null($data)) {
            abort(404);
        }

        $data->teamLokasi = TeamLokasi::with("lokasi")->where('team_id', $data->id)->get();
        $data->teamBooth = TeamBooth::with("booth")->where('team_id', $data->id)->get();
        $data->teamPeserta = TeamPeserta::join("peserta", "peserta.id", "team_peserta.peserta_id")->where('team_peserta.team_id', $data->id)->select("team_peserta.*", "peserta.nama")->get();
        $isEdit = true;

        $lokasi = AmazingRaceLokasi::where('event_id', auth()->user()->id_event)->where('status', 'aktif')->select('id', 'nama')->get();
        $booth = BoothModel::where('event_id', auth()->user()->id_event)->where('status', 'aktif')->select('id', 'nama')->get();

        return view('pages.amazing-race.detail', ['data' => $data, 'lokasi' => $lokasi, 'booth' => $booth, 'isEdit' => $isEdit]);
    }

    public function updateGroup(Request $request) {
        $hasil = [
            'is_success' => false,
            'response_code'  => 400,
            'msg'   => ''
        ];

        DB::beginTransaction();
        try {
            $id = $request->input('id'); 
            $data = $request->only(['nomor_mobil']);

            Team::where('id', $id)->update($data);
            $hasil['response_code'] = 200;

            $lokasi = $request->input('lokasi');
            $urutan = $request->input('urutan');
            if ($lokasi && count($lokasi) > 0) {
                $arr = [];

                foreach ($lokasi as $key => $value) {
                    array_push($arr, [
                        'team_id' => $id,
                        'lokasi_id' => $value,
                        'urutan' => $urutan[$key],
                    ]);
                }

                TeamLokasi::where('team_id', $id)->delete();
                TeamLokasi::insert($arr);
            }

            $booth = $request->input('booth');
            $urutan = $request->input('booth_urutan');
            if ($booth && count($booth) > 0) {
                $arr = [];

                foreach ($booth as $key => $value) {
                    array_push($arr, [
                        'team_id' => $id,
                        'booth_id' => $value,
                        'urutan' => $urutan[$key],
                    ]);
                }

                TeamBooth::where('team_id', $id)->delete();
                TeamBooth::insert($arr);
            }

            DB::commit();
            $hasil['is_success'] = true;
            $hasil['route'] = route('amazing-race.group');
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::info($th);
            $hasil['msg'] = $th->getMessage();
        }

        return response()->json($hasil, $hasil['response_code']);
    }

    public function lokasi(Request $request) {
        if ($request->ajax()) {
            DB::statement(DB::raw('set @rownum=0'));
            $data = AmazingRaceLokasi::where('event_id', auth()->user()->id_event)
                            ->select("id", "nama", "status", "alamat", DB::raw("@rownum  := @rownum  + 1 AS rownum"))
                            ->where('status', 'aktif')
                            ->orderBy('nama', 'asc')->get();
            return DataTables::of($data)
                    ->addIndexColumn()
                    ->setRowAttr([
                        'data-id' => function ($row) {
                            return $row->id;
                        },
                        'data-route' => function($row){
                            return route('amazing-race.lokasi-detail', ['id' => $row->id]);
                        },
                        "data-toggle" => 'showModal',
                        'class' => function($row) {
                            return 'pointer';
                        },
                    ])
                    ->make(true);
        }
        $totalActive = AmazingRaceLokasi::where('event_id', auth()->user()->id_event)->where('status', 'aktif')->count();
        return view('pages.amazing-race.lokasi', ['total' => $totalActive]);
    }

    public function createLokasi(Request $request) {
        $isEdit = false;
        return view('pages.amazing-race.form-lokasi', ['isEdit' => $isEdit]);
    }

    public function detailLokasi($id) {
        $isEdit = true;
        DB::enableQueryLog();
        $data = AmazingRaceLokasi::with('foto')->where('id', $id)->first();
        if(is_null($data)) {
            abort(404);
        }
        $data->foto = AmazingRaceLokasiFoto::where('lokasi_id', $id)->get();
        return view('pages.amazing-race.form-lokasi', ['isEdit' => $isEdit, 'data' => $data]);
    }

    public function storeLokasi(Request $request) {
        $hasil = [
            'is_success' => false,
            'response_code'  => 400,
            'msg'   => ''
        ];

        DB::beginTransaction();
        try {
            $id = $request->input('id'); 
            $data = $request->only(['nama', 'deskripsi', 'kegiatan', 'barang', 'status', 'alamat', 'koordinat', 'jumlah_barang', "durasi_kegiatan"]);

            if ($id != '') {
                AmazingRaceLokasi::where('id', $id)->update($data);
                $hasil['response_code'] = 200;
                $data['id'] = $id;
            } else {
                $data['id'] = Str::uuid();
                $data['event_id'] = auth()->user()->id_event;
                $data['status'] = "aktif";
                AmazingRaceLokasi::create($data);
                $hasil['response_code'] = 201;
            }

            $foto = $request->input('foto');
            if ($foto && count($foto) > 0) {
                $arr = [];

                foreach ($foto as $key => $value) {
                    if (Storage::exists("public/files/tmp1/".$value)) {
                        Storage::move("public/files/tmp1/".$value, "public/files/".$value);
                    }

                    array_push($arr, [
                        'lokasi_id' => $data['id'],
                        'file' => $value
                    ]);
                }

                AmazingRaceLokasiFoto::where('lokasi_id', $data['id'])->delete();
                AmazingRaceLokasiFoto::insert($arr);
            }

            DB::commit();
            $hasil['is_success'] = true;
            $hasil['route'] = route('amazing-race.lokasi');
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::info($th);
            $hasil['msg'] = $th->getMessage();
        }

        return response()->json($hasil, $hasil['response_code']);
    }

    public function storeTmp(Request $request) {
        $hasil = [
            'is_success' => false,
            'response_code'  => 400,
            'msg'   => ''
        ];   

        try {
            if ($request->file('file') && !empty($request->file('file'))) {
                checkDir("files/tmp1");
                $image = $request->file('file');
                $destinationPath = 'storage/files/tmp1/';
                $nameFile = Str::uuid().'.'.$image->getClientOriginalExtension();
                $image->move($destinationPath, $nameFile);
                $hasil['files'] = [
                    'name' => $nameFile,
                    'url' => asset('storage/files/tmp1/'.$nameFile)
                ];
                $hasil['is_success'] = true;
                $hasil['response_code'] = 200;
            }

        } catch (\Throwable $th) {
            Log::info($th);
            $hasil['msg'] = $th->getMessage();
        }

        return response()->json($hasil, $hasil['response_code']);
    }

}
