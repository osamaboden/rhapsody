<?php

namespace App\Exports;

use App\Models\PembelianOtsModel;
use App\Models\TiketModel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\WithEvents;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ExportPembelianOTS implements FromCollection, WithHeadings, WithTitle, WithColumnWidths, WithStyles, WithColumnFormatting
{
    /**
     * @return \Illuminate\Support\Collection
     */

    private $data = [];
    private $title = 'Daftar Pembelian OTS';
    public function __construct($loket)
    {
        DB::statement("SET lc_time_names = 'id_ID';");
        $this->data = PembelianOtsModel::
        when($loket, function($query, $params) {
            if ($params) {
                $query->where('loket', $params);
            }
        })
        ->selectRaw("nama, IF(email != '', email, '-') as email, IF(no_hp != '', no_hp, '-') as no_hp, metode_bayar,IF(kode_booking != '', kode_booking, '-') as kode_booking, jumlah_tiket,CONCAT(DATE_FORMAT(created_at, '%d %b %Y %H:%i'), ' ') as waktu_beli, IF(loket != '', loket, '-') as loket")->orderBy('created_at', 'desc')->get();
    }

    public function collection()
    {
        return $this->data;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function columnFormats(): array
    {
        return [
            'A' => "@",
            'B' => "@",
            'C' => "@",
            'D' => "@",
            'E' => '@',
            'F' => 0,
            'G' => '@',
            'H' => 0,
        ];
    }

    public function columnWidths(): array
    {
        return [
            'A' => 20,
            'B' => 20,
            'C' => 20,
            'D' => 20,
            'E' => 20,
            'F' => 20,
            'G' => 20,
            'H' => 20,
            // 'I' => 20,           
        ];
    }

    public function styles(Worksheet $sheet)
    {
        $i = 1;
        foreach (range('A', "H") as $item) {
            $sheet->getStyle($item.'1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        }
        foreach ($this->data as $key => $value) {
            $i++;
            foreach (range('A', "H") as $item) {
                $sheet->getStyle($item.$i)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
            }
        }

        $sheet->getStyle('A1:H' . $i)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);
    }

    public function headings(): array
    {
        return [
            'Nama',
            'Email',
            'No. HP',
            'Metode Bayar',
            'Kode Booking',
            'Jumlah',
            'Waktu Beli',
            'Loket',
        ];
    }
}
