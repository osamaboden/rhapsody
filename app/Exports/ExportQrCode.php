<?php

namespace App\Exports;

use App\Models\TiketModel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithDrawings;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ExportQrCode implements FromCollection, WithHeadings, WithTitle, WithColumnWidths, WithDrawings, WithStyles
{
    /**
    * @return \Illuminate\Support\Collection
    */
    private $data = [];
    private $title = 'Daftar QR Code Undangan';
    public function __construct($status, $type) {
        if ($type == "umum") {
            $this->title = "Daftar QR Code Umum";
        }
        DB::statement("SET lc_time_names = 'id_ID';");
        $this->data = TiketModel::when($status, function ($query, $params) {
            if ($params == 'not-claim') {
                $query->where('status', 'belum');
            } else if ($params == 'claim') {
                $query->where('status', 'claim');
            } else if ($params == 'keluar') {
                $query->where('status', 'keluar');
            }
        })
        ->when($type, function($query, $params) {
            if ($params == 'umum') {
                $query->limit('4000')->offset('1000');
            } else {
                $query->limit('1000')->offset('0');
            }
        })
        ->selectRaw('kode, IF(waktu_keluar != "", DATE_FORMAT(waktu_keluar, "%d %b %Y %H:%i"), "-") as waktu_keluar, IF(loket_keluar != "", loket_keluar, "-") as loket_keluar, DATE_FORMAT(waktu_redemp, "%d %b %Y %H:%i"), "-") as waktu_redemp, IF(loket != "", loket, "-") as loket, IF(status="belum", "Belum", "Claim") as status, "" as qrcode')
        ->orderByRaw("increment ASC")->get();
    }

    public function collection()
    {
        return $this->data;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function columnWidths(): array
    {
        return [
            'A' => 20,           
            'B' => 20,           
            'C' => 20,         
            'D' => 20,         
            'E' => 20,         
            'F' => 20,         
            'G' => 30,         
            // 'I' => 20,           
        ];
    }

    public function drawings()
    {
        $drawList = [];
        $i = 2;
        foreach ($this->data as $key => $value) {
            $drawing = new Drawing();
            $drawing->setName('qrCode'.$value->kode);
            $drawing->setDescription($value->kode);
            $drawing->setPath(public_path('/storage/qr_code/'.$value->kode.'.png'));
            $drawing->setHeight(70);
            $drawing->setCoordinates('G'.$i);
            $drawing->setOffsetY(8);
            $drawing->setOffsetX(45);
            array_push($drawList, $drawing);
            $i++;
        }

        return $drawList;
    }

    public function styles(Worksheet $sheet)
    {
        $i = 1;
        $sheet->getStyle('A1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        foreach ($this->data as $key => $value) {
            $i++;
            $sheet->getRowDimension($i)->setRowHeight(75);
            $sheet->getStyle('A'.$i)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
            $sheet->getStyle('B'.$i)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
            $sheet->getStyle('C'.$i)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
            $sheet->getStyle('D'.$i)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
            $sheet->getStyle('E'.$i)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
            $sheet->getStyle('F'.$i)->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
            $sheet->getStyle('G'.$i)->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        }

        $sheet->getStyle('A1:G'.$i)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);
    }

    public function headings(): array
{
       return [
        'Kode',
        'Waktu Keluar',
        'Loket',
        'Waktu Redemp',
        'Gate',
        'Status',
        'QR Code',
       ];
    }
}
