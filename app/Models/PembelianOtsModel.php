<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PembelianOtsModel extends Model
{
    use HasFactory;
    protected $table = 'pembelian_ots';
    protected $primaryKey = "id";
    protected $guarded = [];
    public $incrementing = false;
}
