import { checkNavActive, showLoading, showWarning } from "./component.js";
var onNav = false;
var sectionReRegistration = $("#sectionRe-registration");
var listData = [];
const pesertaList = $('#pesertaList')
const type = sectionReRegistration.length > 0 ? sectionReRegistration.attr('data-type') : 'checkout'

$.ajaxSetup({
    headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
    },
});
const cameraPreview = document.getElementById("cameraPreview");
var codeReader = "";
var onCheckData = false;
const checkCode = (
    formData,
    onSuccess = () => { },
    onFailure = () => { }
) => {
    var formAction = sectionReRegistration.length > 0 ? $("#formRegisTration").attr("action") : $('#modalScan').attr('action');
    $.ajax({
        url: formAction,
        type: "POST",
        data: formData,
        beforeSend: async function () {
            showLoading(true);
        },
        success: function (res) {
            onSuccess();
            showLoading(false).then((e) => {
                // <div style="font-size: 15px; font-weight: bold;text-align: center;margin-top: 15px;">Rincian Tiket</div>
                if (res.is_success) {
                    $("#uniqCode").val("");
                    var tabel = '';
                    showWarning({
                        type: "success",
                        hasInput: true,
                        inputElement: ``,
                        saveElement: () => {},
                        title: "Berhasil",
                        msg: type == 'redemp' ?  `Tiket berhasil di redemp` : `Tiket telah keluar`,
                    });
                } else {
                    onSuccess();
                    if (res.error == "tiket-not-found") {
                        showWarning({
                            type: "failed",
                            title: "Gagal",
                            msg: "Tidak menemukan tiket",
                        });
                    } else if (res.error == "tiket-has-claim") {
                        showWarning({
                            type: "failed",
                            title: "Gagal",
                            msg: "Tiket telah di redemp sebelumnya",
                        });
                    } else if (res.error == 'tiket-has-out') {
                        showWarning({
                            type: "failed",
                            title: "Gagal",
                            msg: "Tiket telah keluar sebelumnya",
                        });
                    } else {
                        showWarning({
                            type: "failed",
                            title: "Gagal",
                            msg: "Silakan periksa kembali Kode atau QR Code yang Anda masukkan",
                        });
                    }
                }
            });
        },
        error: function () {
            onSuccess();
            showLoading(false).then((e) => {
                showWarning({
                    type: "failed",
                    title: "Registrasi Gagal",
                    msg: "Silakan periksa kembali Kode Unik atau QR Code yang Anda masukkan",
                });
            });
        },
    });
};

$(".number").mask("000000000000000000000000");

$("#formRegisTration").on("submit", function (e) {
    e.preventDefault();
    var code = $("#uniqCode").val();
    if (code) {
        var data = listData.filter((e) => e.kode == code);
        if (data.length == 0) {
            showWarning({
                type: "failed",
                title: "Data Tidak Ditemukan",
                msg: "Silakan periksa kembali kode yang anda masukan",
            });
        } else {
            checkCode({ uniq_code: code, type: type })
        }

        // 
    }
});

function enterFullScreen(element) {
    if (element.requestFullscreen) {
        element.requestFullscreen();
    } else if (element.mozRequestFullScreen) {
        element.mozRequestFullScreen(); // Firefox
    } else if (element.webkitRequestFullscreen) {
        element.webkitRequestFullscreen(); // Safari
    } else if (element.msRequestFullscreen) {
        element.msRequestFullscreen(); // IE/Edge
    }
}

$("#btnScan").click(function () {
    cameraStream();
    $("#modalScan").modal({
        keyboard: false,
        backdrop: "static",
    });

    $("#modalScan").on("hidden.bs.modal", function () {
        stopCamera();
        if (codeReader) {
            codeReader.stopContinuousDecode();
        }
    });
});

$("#removeButton").click(function () {
    $("#modalScan").modal("hide");
    
    $('#uniqCode').focus()
});

const stopCamera = () => {
    var mediaStream = cameraPreview.srcObject;
    if (mediaStream) {
        // Get all tracks in the stream
        const tracks = mediaStream.getTracks();

        // Stop each track
        tracks.forEach((track) => {
            track.stop();
        });

        // Clear the stream object
        cameraPreview.srcObject = null;
    }
};

const cameraStream = () => {
    if (navigator.mediaDevices.getUserMedia !== undefined) {
        navigator.mediaDevices
            .getUserMedia({ video: true })
            .then((stream) => {
                // Set the video stream as the source of the video element
                cameraPreview.srcObject = stream;
                cameraPreview.play();

                codeReader = new ZXing.BrowserQRCodeReader();
                // codeReader = new ZXing.BrowserBarcodeReader();

                codeReader.decodeFromVideoDevice(
                    null,
                    "cameraPreview",
                    (result, err) => {
                        if (result && !onCheckData) {
                            $('#modalScan').modal('hide');
                            onCheckData = true;
                            cameraPreview.pause();
                            codeReader.stopContinuousDecode();
                            // var split = result.text.split("/");
                            const kodeUnik = result.text;
                            if (kodeUnik) {
                                checkCode(
                                    {
                                        uniq_code: kodeUnik,
                                        type: type
                                    },
                                    () => {
                                        // stopCamera();
                                        // $("#modalScan").modal("hide");
                                        onCheckData = false;
                                    },
                                    () => {
                                        // stopCamera();
                                        // $("#modalScan").modal("hide");
                                        onCheckData = false;
                                    }
                                );
                            }
                            // console.log("QR code detected: ", result.text);
                        }

                        if (err) {
                            if (err instanceof ZXing.NotFoundException) {
                                console.log("No QR code found.");
                                //   document.getElementById('result').textContent = 'No QR code found.'
                            }

                            if (err instanceof ZXing.ChecksumException) {
                                console.log(
                                    "A code was found, but it's read value was not valid."
                                );
                                //   document.getElementById('result').textContent = 'A code was found, but it\'s read value was not valid.'
                            }

                            if (err instanceof ZXing.FormatException) {
                                console.log(
                                    "A code was found, but it was in a invalid format."
                                );
                                //   document.getElementById('result').textContent = 'A code was found, but it was in a invalid format.'
                            }
                        }
                    }
                );
            })
            .catch((error) => {
                console.error("Could not access camera: ", error);
            });
    } else {
    }
};

const getDataAll = () => {
    $.ajax({
        url: '/tiket-all',
        tyoe: "GET",
        beforeSend: function(){
            showLoading(true);
        },success: function(res) {
            showLoading(false);
            listData = res.list;
        }
    })
}

$(document).ready(function(){
    if (sectionReRegistration.length > 0) {
        getDataAll(); 
        if (typeof document.hidden !== "undefined") {
            // Handle page visibility change
            $(document).on("visibilitychange", function() {
                if (document.hidden) {
                    // User switched to another tab or minimized the browser window
                    console.log("User is not in the current tab");
                    // You can perform actions here, like pausing animations or timers
                } else {
                    // User returned to the current tab
                    setTimeout(() => {
                        $('#uniqCode').focus();
                    }, 100);
                    // You can resume animations or timers here
                }
            });
        } else {
            // Page Visibility API is not supported
            console.log("Page Visibility API is not supported");
            // Fallback to alternative approach
        }
    }
})