const checkNavActive = () => {
    var currentPosition = window.scrollY;
    var lineupSection = $("#section-lineup").offset().top - 100;
    var mapsSection = $("#section-maps").offset().top - 130;
    var registrationSection = $("#section-registration").offset().top - 100;
    var sponsorSection = $("#section-sponsor").offset().top - 70;
    var rundownection = $("#section-rundown").offset().top - 40;

    if (currentPosition >= sponsorSection) {
        $(".nav-item .nav-link").removeClass("active");
        $("#nav-sponsor").addClass("active");
    } else if (currentPosition >= registrationSection) {
        $(".nav-item .nav-link").removeClass("active");
        $("#nav-regist").addClass("active");
    } else if (currentPosition >= rundownection) {
        $(".nav-item .nav-link").removeClass("active");
        $("#nav-rundown").addClass("active");
    } else if (currentPosition >= mapsSection) {
        $(".nav-item .nav-link").removeClass("active");
        $("#nav-maps").addClass("active");
    } else if (currentPosition >= lineupSection) {
        $(".nav-item .nav-link").removeClass("active");
        $("#nav-lineUp").addClass("active");
    } else {
        $(".nav-item .nav-link").removeClass("active");
        $("#nav-home").addClass("active");
    }
};

const generateId = () => {
    var k = Math.floor(Math.random() * 1000000);
    var m = k;
    return m;
};

const showWarning = (
    optionData = {
        type: "success",
        title: "",
        msg: "",
        hasInput: false,
        inputElement: "",
        saveElement: () => {

        }
    }
) => {
    var modalId = `modalId${generateId()}`;
    const successIcon = `
        <svg width="100" height="100" viewBox="0 0 178 178" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="89" cy="89" r="89" fill="#F0FFF0" />
            <circle cx="89.0004" cy="89" r="68.1235" fill="#C8FFC7" />
            <circle cx="89.0002" cy="89" r="50.5432" fill="url(#paint0_linear_102_570)" />
            <path d="M112 73.6875L80.375 105.312L66 90.9375" stroke="white" stroke-width="14"
                stroke-linecap="round" stroke-linejoin="round" />
            <defs>
                <linearGradient id="paint0_linear_102_570" x1="89.0002" y1="38.4568" x2="89.0002" y2="139.543"
                    gradientUnits="userSpaceOnUse">
                    <stop stop-color="#69F45D" />
                    <stop offset="1" stop-color="#1A9E28" />
                </linearGradient>
            </defs>
        </svg>
    `;
    const failedIcon = `
        <svg width="100" height="100" viewBox="0 0 162 162" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="81" cy="81" r="81" fill="#FFF8F8" />
            <circle cx="81" cy="81" r="62" fill="#FAD2DA" />
            <circle cx="81" cy="81" r="46" fill="url(#paint0_linear_74_677)" />
            <path d="M94 68L68 94" stroke="white" stroke-width="12" stroke-linecap="round"
                stroke-linejoin="round" />
            <path d="M68 68L94 94" stroke="white" stroke-width="12" stroke-linecap="round"
                stroke-linejoin="round" />
            <defs>
                <linearGradient id="paint0_linear_74_677" x1="81" y1="35" x2="81" y2="127"
                    gradientUnits="userSpaceOnUse">
                    <stop stop-color="#FF7D89" />
                    <stop offset="1" stop-color="#DD1D37" />
                </linearGradient>
            </defs>
        </svg>
    `;

    var html = `
    <div class="modal fade" id="${modalId}" tabindex="-1">
        <div class="modal-dialog modal-error modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body text-center">
                    ${optionData.type == "success" ? successIcon : failedIcon}
                    <div class="modal-title mt-3">
                        ${optionData.title}
                    </div>
                    <div class="modal-desc mt-2">
                         ${optionData.msg}
                         ${optionData.hasInput ? optionData.inputElement : ''}
                    </div>
                </div>
                <div class="modal-footer tex-center justify-content-center">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>    
    `;

    $(".main-wrapper").append(html);

    setTimeout(() => {
        $(`#${modalId}`).modal("show");
        $(`#${modalId}`).on("hidden.bs.modal", function () {
            if (optionData.hasInput) {
                optionData.saveElement();
            }
            setTimeout(() => {
                $("body").find(`#${modalId}`).remove();
                $('#uniqCode').val('').focus();
            }, 100);
        });
    }, 300);
};

const showLoading = (isShow = true) => {
    return new Promise((resolve, reject) => {
        if (isShow) {
            var loading = `
            <div class="modal fade show" id="loadingModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: block;">
                <div role="document" class="modal-dialog modal-dialog-centered">
                    <div class="modal-content" style="background: unset;box-shadow: unset;">
                        <div class="modal-body" style="color: white;font-size: 22px;text-align: center;">
                            <i class="fas fa-spinner fa-spin" style="font-size: 50px;color: white;"></i>
                            <div>Please wait...</div>
                        </div>
                    </div>
                </div>
            </div>
            `;
            $("body").append(loading);
            setTimeout(() => {
                $("#loadingModal").modal({
                    show: true,
                    focus: true,
                    backdrop: "static",
                    keyboard: false,
                });
                resolve(true);
            }, 200);
        } else {
            setTimeout(() => {
                $("#loadingModal").modal("hide");
                setTimeout(() => {
                    $("#loadingModal").remove();
                    $('#uniqCode').focus();
                    resolve(true);
                }, 200);
            }, 700);
        }
    });
};

export { checkNavActive, showWarning, showLoading };
