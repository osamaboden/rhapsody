import { checkNavActive, showLoading, showWarning } from "./component.js";
var onNav = false;
var sectionReRegistration = $("#sectionRe-registration");

$.ajaxSetup({
    headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
    },
});

if (sectionReRegistration.length > 0) {
    const cameraPreview = document.getElementById("cameraPreview");
    var codeReader = "";
    var onCheckData = false;
    const checkCode = (
        uniqCode,
        onSuccess = () => {},
        onFailure = () => {}
    ) => {
        var formAction = $("#formRegisTration").attr("action");

        $.ajax({
            url: formAction,
            type: "POST",
            data: {
                uniq_code: uniqCode.toLowerCase().toUpperCase(),
            },
            beforeSend: async function () {
                showLoading(true);
            },
            success: function (res) {
                onSuccess();
                showLoading(false).then((e) => {
                    if (res.is_success) {
                        $("#uniqCode").val("");
                        showWarning({
                            type: "success",
                            title: 'Check In <span style="color: #45D42E"></span>',
                            msg: "Please enter and enjoy the event",
                        });
                    } else {
                        onSuccess();
                        showWarning({
                            type: "failed",
                            title: 'Check In <span style="color:#ff2c00;">Failed</span>',
                            msg: "Please check again the Unique Code or QR Code that you entered",
                        });
                    }
                });
            },
            error: function () {
                onSuccess();
                showLoading(false).then((e) => {
                    showWarning({
                        type: "failed",
                        title: 'Check In <span style="color:#ff2c00;">Failed</span>',
                        msg: "Please check again the Unique Code or QR Code that you entered",
                    });
                });
            },
        });
    };

    $("#formRegisTration").on("submit", function (e) {
        e.preventDefault();
        var code = $("#uniqCode").val();
        checkCode(code);
    });

    function enterFullScreen(element) {
        if (element.requestFullscreen) {
            element.requestFullscreen();
        } else if (element.mozRequestFullScreen) {
            element.mozRequestFullScreen(); // Firefox
        } else if (element.webkitRequestFullscreen) {
            element.webkitRequestFullscreen(); // Safari
        } else if (element.msRequestFullscreen) {
            element.msRequestFullscreen(); // IE/Edge
        }
    }

    $("#btnScan").click(function () {
        cameraStream();
        $("#modalScan").modal({
            keyboard: false,
            backdrop: "static",
        });

        $("#modalScan").on("hidden.bs.modal", function () {
            stopCamera();
            if (codeReader) {
                codeReader.stopContinuousDecode();
            }
        });
    });

    $("#removeButton").click(function () {
        $("#modalScan").modal("hide");
    });

    const stopCamera = () => {
        var mediaStream = cameraPreview.srcObject;
        if (mediaStream) {
            // Get all tracks in the stream
            const tracks = mediaStream.getTracks();

            // Stop each track
            tracks.forEach((track) => {
                track.stop();
            });

            // Clear the stream object
            cameraPreview.srcObject = null;
        }
    };

    const cameraStream = () => {
        if (navigator.mediaDevices.getUserMedia !== undefined) {
            navigator.mediaDevices
                .getUserMedia({ video: true })
                .then((stream) => {
                    // Set the video stream as the source of the video element
                    cameraPreview.srcObject = stream;
                    cameraPreview.play();

                    codeReader = new ZXing.BrowserQRCodeReader();

                    codeReader.decodeFromVideoDevice(
                        null,
                        "cameraPreview",
                        (result, err) => {
                            if (result && !onCheckData) {
                                onCheckData = true;
                                cameraPreview.pause();
                                codeReader.stopContinuousDecode();
                                checkCode(
                                    result.text,
                                    () => {
                                        stopCamera();
                                        $("#modalScan").modal("hide");
                                        onCheckData = false;
                                    },
                                    () => {
                                        stopCamera();
                                        $("#modalScan").modal("hide");
                                        onCheckData = false;
                                    }
                                );
                                // console.log('QR code detected: ', result.text);
                                // alert(result.text);
                            }

                            if (err) {
                                if (err instanceof ZXing.NotFoundException) {
                                    console.log("No QR code found.");
                                    //   document.getElementById('result').textContent = 'No QR code found.'
                                }

                                if (err instanceof ZXing.ChecksumException) {
                                    console.log(
                                        "A code was found, but it's read value was not valid."
                                    );
                                    //   document.getElementById('result').textContent = 'A code was found, but it\'s read value was not valid.'
                                }

                                if (err instanceof ZXing.FormatException) {
                                    console.log(
                                        "A code was found, but it was in a invalid format."
                                    );
                                    //   document.getElementById('result').textContent = 'A code was found, but it was in a invalid format.'
                                }
                            }
                        }
                    );
                })
                .catch((error) => {
                    console.error("Could not access camera: ", error);
                });
        } else {
        }
    };
} else {
    $(window).scroll(function () {
        var $this = $(this);
        let navbar = $(".navbar-wrapper");
        let windowPosition = window.scrollY > 9;
        navbar.toggleClass("scrolling-active", windowPosition);
        if (!onNav) {
            checkNavActive();
        }
        // if ($this.scrollTop() < 1) {
        //     $(".nav-link").removeClass("active");
        //     $("#nav-home").addClass("active");
        // } else {
        //     $(".nav-link").removeClass("active");
        //     $("#nav-home").removeClass("active");
        // }
    });

    $(window).ready(function () {
        if (!location.pathname.includes("/crew/template-event/preview")) {
            checkNavActive();
        }
        $(".phone").mask("000000000000000000");
        let windowPosition = window.scrollY > 9;
        if (windowPosition) {
            let navbar = $(".navbar-wrapper");
            navbar.toggleClass("scrolling-active", windowPosition);
        }

        $(".navbar-wrapper .nav-item .nav-link").click(function () {
            $(".nav-item .nav-link").removeClass("active");
            $(this).addClass("active");
            onNav = true;
            $("html, body").animate(
                {
                    scrollTop: $($(this).attr("data-target")).offset().top - 30,
                },
                500
            );
            setTimeout(() => {
                onNav = false;
            }, 550);
        });

        $(".to_link").click(function () {
            onNav = true;
            $("html, body").animate(
                {
                    scrollTop: $($(this).attr("data-target")).offset().top - 30,
                },
                500
            );
            setTimeout(() => {
                onNav = false;
            }, 550);
        });

        $(".navbar-toggler").click(function () {
            $(".navbar-collapse").toggleClass("show");
        });
    });

    $("#registrationForm").submit(function (e) {
        var buttonSubmit = $(this).find('[type="submit"]');
        e.preventDefault();
        var formData = $(this).serialize();
        $.ajax({
            url: $(this).attr("action"),
            data: formData,
            type: "POST",
            beforeSend: function () {
                buttonSubmit.attr("disabled", true);
                buttonSubmit.html(
                    '<i class="fas fa-spinner fa-spin" style="font-size: 25px;"></i> Loading'
                );
            },
            success: function (res) {
                buttonSubmit.attr("disabled", false);
                buttonSubmit.html("Get Ticket");
                if (res.is_success) {
                    location.href = res.toRoute;
                } else {
                    if (res.error == "quota has been exceeded") {
                        showWarning({
                            type: "error",
                            title: 'Sorry, Your Registration <span style="color:#ff2c00;">Failed</span>',
                            msg: "Quota has been exceeded.",
                        });
                    } else if (res.error == "has been used") {
                        showWarning({
                            type: "error",
                            title: 'Sorry, Your Registration <span style="color:#ff2c00;">Failed</span>',
                            msg: "Email and phone number have been used before, please use another email and phone number.",
                        });
                    } else {
                        showWarning({
                            type: "error",
                            title: 'Sorry, Your Registration <span style="color:#ff2c00;">Failed</span>',
                            msg: "",
                        });
                    }
                }
            },
            error: function () {
                buttonSubmit.attr("disabled", false);
                buttonSubmit.html("Get Ticket");
                showWarning({
                    type: "error",
                    title: 'Sorry, Your Registration <span style="color:#ff2c00;">Failed</span>',
                    msg: "",
                });
            },
        });
    });
}
