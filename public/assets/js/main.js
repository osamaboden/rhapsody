$(document).ready(function () {
    const btnEdit = $("#btn-edit");
    const btnSave = $("#btn-save");

    btnEdit.click(function () {
        btnSave.removeClass("d-none");
        btnEdit.addClass("d-none");
        $("input").removeAttr("disabled");
        $("select").removeAttr("disabled");
    });
});
