<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeContoller;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\BoothController;
use App\Http\Controllers\KamarController;
use App\Http\Controllers\QueueController;
use App\Http\Controllers\ContentController;
use App\Http\Controllers\RundownController;
use App\Http\Controllers\SeatingController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ReservasiController;
use App\Http\Controllers\AmazingRaceController;
use App\Http\Controllers\ParticipantController;
use App\Http\Controllers\EventGalleryController;
use App\Http\Controllers\CustomContestController;
use App\Http\Controllers\DestinasiWisataController;
use App\Http\Controllers\NominateController;
use App\Http\Controllers\PembelianOtsController;
use App\Http\Controllers\QrCodeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('crew')->group(function () {
    Route::get('/', [AuthController::class, 'index'])->name('login');
    Route::post('login', [AuthController::class, 'aksi_login'])->name('aksilogin');
    Route::get('logout', [AuthController::class, 'aksi_logout'])->name('aksilogout');

    Route::get('/setup-password/{kode}', [AuthController::class, 'setupPassword'])->name("setup-password");
    Route::post('/setup-password/save', [AuthController::class, 'savePassword'])->name("save-password");

    Route::group(['middleware' => 'auth:web'], function () {
        Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard.index');

        Route::get('/qrcode', [QrCodeController::class, 'index'])->name('qrcode.index');
        Route::get('/qrcode/export', [QrCodeController::class, 'export'])->name('qrcode.export');

        Route::get('/redemp-tiket', [HomeContoller::class, 'checkin'])->name('redemp.tiket');
        Route::get('/checkout-tiket', [HomeContoller::class, 'checkout'])->name('checkout.tiket');

        Route::get('/pembelian-ots', [PembelianOtsController::class, 'index'])->name('pembelian_ots.index');
        Route::get('/pembelian-ots/export', [PembelianOtsController::class, 'export'])->name('pembelian_ots.export');
        Route::post('/pembelian-ots/store', [PembelianOtsController::class, 'store'])->name('pembelian_ots.store');
    });
});

Route::get('/', [AuthController::class, 'index'])->name('home');
Route::post('/re-registration/check', [HomeContoller::class, 're_registrationCheck'])->name('re-registration.check');
Route::get('/tiket-all', [HomeContoller::class, 'pesertaAll'])->name('regis-perserta');
